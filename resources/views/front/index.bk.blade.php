@extends('layouts.front')

@section('cssExtras')

@endsection
@section('jsLibExtras')

@endsection
@section('styleExtras')

@endsection
@section('content')
	<div class="uk-container uk-container-expand uk-margin-remove">
		<div class="uk-width-1-1 mar-pad-r pad-2-0">
			<div class="uk-width-1-1 mar-pad-r uk-slider" uk-slider="" style="">
				<div class="uk-width-1-1 mar-pad-r uk-position-relative" style="position:relative;z-index:1">
					<div class="uk-width-1-1 mar-pad-r uk-slider-container uk-light">
						<ul class="uk-width-1-1 mar-pad-r uk-slider-items uk-child-width-1-1" style="transform: translate3d(0px, 0px, 0px);">
							<li class="uk-width-1-1 mar-pad-r height-560 uk-active" tabindex="-1">
								<div class="uk-height-1-1 uk-width-1-1 uk-flex uk-flex-center uk-flex-middle uk-background-cover uk-light height-560" data-src="{{asset('img/design/banner1.jpg')}}" uk-img=""
								 style="z-index: 1; background-image: url({{asset('img/design/banner1.jpg')}});"> </div>
							</li>
							<li class="uk-width-1-1 mar-pad-r height-560" tabindex="-1">
								HOLA
							</li>
							<li class="uk-width-1-1 mar-pad-r height-560" tabindex="-1">
								HOLA
							</li>
						</ul>
					</div>
					<!-- <div class="">
							            <a class="uk-position-center-left-out uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
							            <a class="uk-position-center-right-out uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
							        </div> -->
				</div>
				<div class="mar-pad-r cont-ul">
					<ul class="uk-padding-remove uk-slider-nav uk-dotnav uk-flex-right uk-margin">
						<li uk-slider-item="0" class="uk-active"><a href=""></a></li>
						<li uk-slider-item="1"><a href=""></a></li>
						<li uk-slider-item="2"><a href=""></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<h1 class="uk-text-center space4 txt-30 pad-15"> DESTACADOS </h1>
	<div class="uk-container uk-container-expand uk-margin-remove">
		<div class="uk-width-1-1 mar-pad-r" style="padding:0!important;">
			<div class="uk-width-1-1 mar-pad-r uk-slider uk-slider-container" uk-slider="">
				<div class="uk-width-1-1 mar-pad-r uk-position-relative">
					<div class="uk-width-1-1 mar-pad-ruk-slider-container uk-light">
						<ul class="uk-width-1-1 mar-pad-r uk-slider-items uk-child-width-1-1 uk-child-width-1-2@s uk-child-width-1-3@m" style="transform: translate3d(0px, 0px, 0px);">
						<li class="uk-margin-remove uk-padding-small uk-active" tabindex="-1">
							<div class="mar-pad-r uk-text-center cont-prod">
								<div class="mar-pad-r uk-inline-clip uk-transition-toggle" tabindex="0" style="">
									<div class="mar-pad-r" style="">
										<div class="height-266">
											<img class="height-266" src="img/design/banner1.jpg" alt="">
										</div>
										<div class="uk-position-small txt-16 negro line">
											<div class="txt-14 bold500 negro"> NOMBRE DE EL PRODUCTO </div>
											<div class="gris pad-5"> $ 45098 </div>
											<div class="txt-card"> Texto descriptivo de el producto, Lorem Ipsum brebe 2 a 3 lineas </div>
										</div>
									</div>
									<div class=" uk-transition-fade uk-position-cover uk-position-small uk-overlay uk-overlay-default uk-flex uk-flex-center uk-flex-middle height-172">
										<p class="uk-h4 uk-margin-remove gris">
											VER DETALLE<br>
										</p>
										<hr class="border-gris">
										<i class="fas fa-shopping-bag i-bolsa gris" aria-hidden="true"></i>
										<hr class="border-gris">
										<p></p>
									</div>
								</div>
								<div class="line-bolsa gris">
									<i class="fas fa-shopping-bag i-bolsa" aria-hidden="true"></i>
								</div>

							</div>
						</li>
						<li class="uk-margin-remove uk-padding-small uk-active" tabindex="-1">
							<div class="mar-pad-r uk-text-center cont-prod">
								<div class="mar-pad-r uk-inline-clip uk-transition-toggle" tabindex="0" style="">
									<div class="mar-pad-r" style="">
										<div class="height-266">
											<img class="height-266" src="img/design/banner1.jpg" alt="">
										</div>
										<div class="uk-position-small txt-16 negro line">
											<div class="txt-14 bold500 negro"> NOMBRE DE EL PRODUCTO </div>
											<div class="gris pad-5"> $ 45098 </div>
											<div class="txt-card"> Texto descriptivo de el producto, Lorem Ipsum brebe 2 a 3 lineas </div>
										</div>
									</div>
									<div class=" uk-transition-fade uk-position-cover uk-position-small uk-overlay uk-overlay-default uk-flex uk-flex-center uk-flex-middle height-172">
										<p class="uk-h4 uk-margin-remove gris">
											VER DETALLE<br>
										</p>
										<hr class="border-gris">
										<i class="fas fa-shopping-bag i-bolsa gris" aria-hidden="true"></i>
										<hr class="border-gris">
										<p></p>
									</div>
								</div>
								<div class="line-bolsa gris">
									<i class="fas fa-shopping-bag i-bolsa" aria-hidden="true"></i>
								</div>

							</div>
						</li>
						<li class="uk-margin-remove uk-padding-small uk-active" tabindex="-1">
							<div class="mar-pad-r uk-text-center cont-prod">
								<div class="mar-pad-r uk-inline-clip uk-transition-toggle" tabindex="0" style="">
									<div class="mar-pad-r" style="">
										<div class="height-266">
											<img class="height-266" src="img/design/banner1.jpg" alt="">
										</div>
										<div class="uk-position-small txt-16 negro line">
											<div class="txt-14 bold500 negro"> NOMBRE DE EL PRODUCTO </div>
											<div class="gris pad-5"> $ 45098 </div>
											<div class="txt-card"> Texto descriptivo de el producto, Lorem Ipsum brebe 2 a 3 lineas </div>
										</div>
									</div>
									<div class=" uk-transition-fade uk-position-cover uk-position-small uk-overlay uk-overlay-default uk-flex uk-flex-center uk-flex-middle height-172">
										<p class="uk-h4 uk-margin-remove gris">
											VER DETALLE<br>
										</p>
										<hr class="border-gris">
										<i class="fas fa-shopping-bag i-bolsa gris" aria-hidden="true"></i>
										<hr class="border-gris">
										<p></p>
									</div>
								</div>
								<div class="line-bolsa gris">
									<i class="fas fa-shopping-bag i-bolsa" aria-hidden="true"></i>
								</div>

							</div>
						</li>
						<li class="uk-margin-remove uk-padding-small" tabindex="-1">
							<div class="mar-pad-r uk-text-center cont-prod">
								<div class="mar-pad-r uk-inline-clip uk-transition-toggle" tabindex="0" style="">
									<div class="mar-pad-r" style="">
										<div class="height-266">
											<img class="height-266" src="img/design/banner1.jpg" alt="">
										</div>
										<div class="uk-position-small txt-16 negro line">
											<div class="txt-14 bold500 negro"> NOMBRE DE EL PRODUCTO </div>
											<div class="gris pad-5"> $ 45098 </div>
											<div class="txt-card"> Texto descriptivo de el producto, Lorem Ipsum brebe 2 a 3 lineas </div>
										</div>
									</div>
									<div class=" uk-transition-fade uk-position-cover uk-position-small uk-overlay uk-overlay-default uk-flex uk-flex-center uk-flex-middle height-172">
										<p class="uk-h4 uk-margin-remove gris">
											VER DETALLE<br>
										</p>
										<hr class="border-gris">
										<i class="fas fa-shopping-bag i-bolsa gris" aria-hidden="true"></i>
										<hr class="border-gris">
										<p></p>
									</div>
								</div>
								<div class="line-bolsa gris">
									<i class="fas fa-shopping-bag i-bolsa" aria-hidden="true"></i>
								</div>

							</div>
						</li>
						<li class="uk-margin-remove uk-padding-small" tabindex="-1">
							<div class="mar-pad-r uk-text-center cont-prod">
								<div class="mar-pad-r uk-inline-clip uk-transition-toggle" tabindex="0" style="">
									<div class="mar-pad-r" style="">
										<div class="height-266">
											<img class="height-266" src="img/design/banner1.jpg" alt="">
										</div>
										<div class="uk-position-small txt-16 negro line">
											<div class="txt-14 bold500 negro"> NOMBRE DE EL PRODUCTO </div>
											<div class="gris pad-5"> $ 45098 </div>
											<div class="txt-card"> Texto descriptivo de el producto, Lorem Ipsum brebe 2 a 3 lineas </div>
										</div>
									</div>
									<div class=" uk-transition-fade uk-position-cover uk-position-small uk-overlay uk-overlay-default uk-flex uk-flex-center uk-flex-middle height-172">
										<p class="uk-h4 uk-margin-remove gris">
											VER DETALLE<br>
										</p>
										<hr class="border-gris">
										<i class="fas fa-shopping-bag i-bolsa gris" aria-hidden="true"></i>
										<hr class="border-gris">
										<p></p>
									</div>
								</div>
								<div class="line-bolsa gris">
									<i class="fas fa-shopping-bag i-bolsa" aria-hidden="true"></i>
								</div>

							</div>
						</li>
						<li class="uk-margin-remove uk-padding-small" tabindex="-1">
							<div class="mar-pad-r uk-text-center cont-prod">
								<div class="mar-pad-r uk-inline-clip uk-transition-toggle" tabindex="0" style="">
									<div class="mar-pad-r" style="">
										<div class="height-266">
											<img class="height-266" src="img/design/banner1.jpg" alt="">
										</div>
										<div class="uk-position-small txt-16 negro line">
											<div class="txt-14 bold500 negro"> NOMBRE DE EL PRODUCTO </div>
											<div class="gris pad-5"> $ 45098 </div>
											<div class="txt-card"> Texto descriptivo de el producto, Lorem Ipsum brebe 2 a 3 lineas </div>
										</div>
									</div>
									<div class=" uk-transition-fade uk-position-cover uk-position-small uk-overlay uk-overlay-default uk-flex uk-flex-center uk-flex-middle height-172">
										<p class="uk-h4 uk-margin-remove gris">
											VER DETALLE<br>
										</p>
										<hr class="border-gris">
										<i class="fas fa-shopping-bag i-bolsa gris" aria-hidden="true"></i>
										<hr class="border-gris">
										<p></p>
									</div>
								</div>
								<div class="line-bolsa gris">
									<i class="fas fa-shopping-bag i-bolsa" aria-hidden="true"></i>
								</div>

							</div>
						</li>
						</ul>
					</div>
					<!-- <div class="u">
	<a class="uk-position-center-left-out uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
	<a class="uk-position-center-right-out uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
	</div> -->
				</div>
				<ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin">
					<li uk-slider-item="0" class="uk-active"><a href=""></a></li>
					<li uk-slider-item="1"><a href=""></a></li>
					<li uk-slider-item="2"><a href=""></a></li>
					<li uk-slider-item="3"><a href=""></a></li>
					<li uk-slider-item="4"><a href=""></a></li>
					<li uk-slider-item="5"><a href=""></a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="uk-container uk-container-small uk-margin-remove uk-padding uk-text-center txt-22 gris">
		Lorem Ipsum has been the industry´s standard dummy text ever since the 150s, when an unknown printer took a galler of type and scrambled it to
	</div>
	<div class="uk-container uk-container-expand uk-margin-remove">
		<div class="uk-width-1-1 mar-pad-r uk-grid-small uk-grid" uk-grid="">
			<div class="uk-width-3-5 uk-margin-remove uk-padding-remove uk-first-column">
				<div class="uk-width-1-1 space4 txt-30 uk-flex uk-flex-right pad-15 title"> ESPACIOS </div>
				<div class="uk-width-1-1 mar-pad-r uk-grid-small uk-grid" uk-grid="">
					<!-- limite de 9 fotos -->
					<div class="uk-width-1-3@m uk-margin-remove uk-padding-remove uk-inline-clip uk-transition-toggle border-blaco uk-first-column" tabindex="0">
						<img class="uk-margin-remove uk-padding-remove" src="img/design/banner1.jpg" alt="" style="height:10em">
						<div class="uk-margin-remove uk-padding-remove uk-transition-fade uk-position-cover uk-position-middle uk-overlay uk-overlay-default uk-flex uk-flex-center uk-flex-middle uk-height-middle" style="">
							<p class="uk-h4 uk-margin-remove txt-16 gris">
								VER DETALLE</p>
						</div>
					</div>
					<!-- limite de 9 fotos -->
					<div class="uk-width-1-3@m uk-margin-remove uk-padding-remove uk-inline-clip uk-transition-toggle border-blaco" tabindex="0">
						<img class="uk-margin-remove uk-padding-remove" src="img/design/banner1.jpg" alt="" style="height:10em">
						<div class="uk-margin-remove uk-padding-remove uk-transition-fade uk-position-cover uk-position-middle uk-overlay uk-overlay-default uk-flex uk-flex-center uk-flex-middle uk-height-middle" style="">
							<p class="uk-h4 uk-margin-remove txt-16 gris">
								VER DETALLE</p>
						</div>
					</div>
					<!-- limite de 9 fotos -->
					<div class="uk-width-1-3@m uk-margin-remove uk-padding-remove uk-inline-clip uk-transition-toggle border-blaco" tabindex="0">
						<img class="uk-margin-remove uk-padding-remove" src="img/design/banner1.jpg" alt="" style="height:10em">
						<div class="uk-margin-remove uk-padding-remove uk-transition-fade uk-position-cover uk-position-middle uk-overlay uk-overlay-default uk-flex uk-flex-center uk-flex-middle uk-height-middle" style="">
							<p class="uk-h4 uk-margin-remove txt-16 gris">
								VER DETALLE</p>
						</div>
					</div>
					<!-- limite de 9 fotos -->
					<div class="uk-width-1-3@m uk-margin-remove uk-padding-remove uk-inline-clip uk-transition-toggle border-blaco uk-grid-margin uk-first-column" tabindex="0">
						<img class="uk-margin-remove uk-padding-remove" src="img/design/banner1.jpg" alt="" style="height:10em">
						<div class="uk-margin-remove uk-padding-remove uk-transition-fade uk-position-cover uk-position-middle uk-overlay uk-overlay-default uk-flex uk-flex-center uk-flex-middle uk-height-middle" style="">
							<p class="uk-h4 uk-margin-remove txt-16 gris">
								VER DETALLE</p>
						</div>
					</div>
					<!-- limite de 9 fotos -->
					<div class="uk-width-1-3@m uk-margin-remove uk-padding-remove uk-inline-clip uk-transition-toggle border-blaco uk-grid-margin" tabindex="0">
						<img class="uk-margin-remove uk-padding-remove" src="img/design/banner1.jpg" alt="" style="height:10em">
						<div class="uk-margin-remove uk-padding-remove uk-transition-fade uk-position-cover uk-position-middle uk-overlay uk-overlay-default uk-flex uk-flex-center uk-flex-middle uk-height-middle" style="">
							<p class="uk-h4 uk-margin-remove txt-16 gris">
								VER DETALLE</p>
						</div>
					</div>
					<!-- limite de 9 fotos -->
					<div class="uk-width-1-3@m uk-margin-remove uk-padding-remove uk-inline-clip uk-transition-toggle border-blaco uk-grid-margin" tabindex="0">
						<img class="uk-margin-remove uk-padding-remove" src="img/design/banner1.jpg" alt="" style="height:10em">
						<div class="uk-margin-remove uk-padding-remove uk-transition-fade uk-position-cover uk-position-middle uk-overlay uk-overlay-default uk-flex uk-flex-center uk-flex-middle uk-height-middle" style="">
							<p class="uk-h4 uk-margin-remove txt-16 gris">
								VER DETALLE</p>
						</div>
					</div>
					<!-- limite de 9 fotos -->
					<div class="uk-width-1-3@m uk-margin-remove uk-padding-remove uk-inline-clip uk-transition-toggle border-blaco uk-grid-margin uk-first-column" tabindex="0">
						<img class="uk-margin-remove uk-padding-remove" src="img/design/banner1.jpg" alt="" style="height:10em">
						<div class="uk-margin-remove uk-padding-remove uk-transition-fade uk-position-cover uk-position-middle uk-overlay uk-overlay-default uk-flex uk-flex-center uk-flex-middle uk-height-middle" style="">
							<p class="uk-h4 uk-margin-remove txt-16 gris">
								VER DETALLE</p>
						</div>
					</div>

					<div class="uk-width-1-1 uk-margin-remove newslatter uk-grid-margin uk-first-column">
						<div class="width-80">
							<h1 class="uk-width-1-1 mar-pad-r space4 txt-30 pad-15 uk-flex uk-flex-center"> SUSCRIBETE A NUESTRO NEWSLATTER </h1>
							<div class="uk-width-1-1 mar-pad-r uk-grid-small uk-grid" uk-grid="">
								<div class="uk-width-1-2 uk-margin-remove pad-0-5 uk-first-column">
									<input type="text" class="uk-input new-input" id="footernombre" placeholder="NOMBRE" style="" autocomplete="off">
								</div>
								<div class="uk-width-1-2 uk-margin-remove pad-0-5">
									<input type="text" class="uk-input new-input" id="footernombre" placeholder="CORREO" style="" autocomplete="off">
								</div>
								<div class="uk-width-1-1 uk-text-center space4 txt-14 bold500 uk-grid-margin uk-first-column"> REGISTRARME </div>
								<p class="uk-margin-remove txt-14 new-text uk-grid-margin uk-first-column">
									Lorem Ipsum has been the industry´s standard dummy text ever since the 150s, when an unknown printer took a galler of type and scrambled it to.
									Lorem Ipsum hasbeen the insdustry's standard dummy test ever since the 1500s, when an unknown printer took a galley of type and scrambled it to
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="uk-width-2-5 mar-pad-r border:solid 4px #fff;">
				<div class="uk-width-1-1 mar-pad-r uk-flex uk-flex-center uk-flex-bottom height-14o5">
					<h1 class="uk-width-1-1 mar-pad-r space4 txt-30 pad-15 uk-flex uk-flex-center titles"> SUBASTAS </h1>
				</div>
				<div class="cont-subasta  subasta">
					<h1 class="uk-width-1-1 mar-pad-r space4 blanco txt-22 titles"> SUBASTAS </h1>
					<p class="txt-12">
						Lorem Ipsum hasbeen the insdustry's standard dummy test ever since the 1500s, when an unknown printer took a galley of type and scrambled it to

					</p>
					<div class="uk-width-1-1 mar-pad-r uk-grid-small uk-flex uk-flex-left uk-flex-middle uk-grid" uk-grid="">
						<hr class="mar-pad-r width-50 uk-first-column">
						<spam class="uk-width-auto"> mas </spam>
					</div>
					<div class="mar-pad-r uk-grid-match width-86 uk-grid uk-grid-stack" uk-grid="">

						<div class="mar-pad-r uk-first-column">
							<div class="uk-card uk-card-default padding-10">
								<div class="uk-card-media-top uk-flex uk-flex-center uk-flex-middle height-210">
									<img src="img/design/banner1.jpg" alt="" style="max-height:210px">
								</div>
								<div class="uk-padding-small txt-14 negro line uk-text-center">
									<div class="txt-14 bold500 negro pad-5"> NOMBRE DE EL PRODUCTO </div>
									<div class="pad-5 txt-card"> Texto descriptivo de el producto, Lorem Ipsum brebe 2 a 3 lineas </div>
									<hr class="mar-pad-r">
									<div class="gris bold500 pad-5">Inicia desde $ 45098 </div>
									<div class="txt-14 bold600 negro pad-5 negro space"> PUJAR </div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('jsLibExtras2')
@endsection
@section('jqueryExtra')
<script type="text/javascript">
</script>
@endsection
