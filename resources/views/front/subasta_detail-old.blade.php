@extends('layouts.front')

@section('cssExtras')

@endsection
@section('jsLibExtras')

@endsection
@section('styleExtras')

@endsection
@section('content')
<section class="uk-margin-remove container-base">

	<div class="uk-offcanvas-content uk-position-relative">
		@include('layouts.partials_front.header')
		<section class="uk-margin-remove uk-padding-remove uk-grid-match" uk-grid>
			<div class="mar-pad-r col-izq">&nbsp;</div>
			<div class="uk-width-expand mar-pad-r uk-flex uk-flex-center cont-center">
				<!-- /* (body) CONTENIDO DE LA VISTA */ -->

				<div class="uk-container uk-container-expand uk-margin-remove">
					<div class="uk-width-1-1 uk-margin-remove uk-grid-small" uk-grid style="padding:60px 0">
						<div class="uk-width-1-2 mar-pad-r">
							<div class="uk-card uk-card-default padding-10">
								<img src="img/design/banner1.jpg" alt="" class="uk-width-1-1 mar-pad-r uk-height-large" style="position:relative; z-index:4;">
								<div class="uk-width-1-1 uk-margin-remove pad-5" uk-slider>
									<div class="uk-width-1-1 mar-pad-r uk-position-relative">
										<div class="uk-width-1-1 mar-pad-ruk-slider-container uk-light">
											<ul class="uk-width-1-1 mar-pad-r uk-slider-items " style="">
												<li class="uk-width-auto mar-pad-r uk-slider-items">
													<img src="img/design/banner1.jpg" alt="" class="mar-pad-r uk-width-small uk-height-small" style="border:1px #fff solid;">
												</li>
												<li class="uk-width-auto mar-pad-r uk-slider-items">
													<img src="img/design/banner1.jpg" alt="" class="mar-pad-r uk-width-small uk-height-small" style="border:1px #fff solid;">
												</li>
												<li class="uk-width-auto mar-pad-r uk-slider-items">
													<img src="img/design/banner1.jpg" alt="" class="mar-pad-r uk-width-small uk-height-small" style="border:1px #fff solid;">
												</li>
												<li class="uk-width-auto mar-pad-r uk-slider-items">
													<img src="img/design/banner1.jpg" alt="" class="mar-pad-r uk-width-small uk-height-small" style="border:1px #fff solid;">
												</li>
												<li class="uk-width-auto mar-pad-r uk-slider-items">
													<img src="img/design/banner1.jpg" alt="" class="mar-pad-r uk-width-small uk-height-small" style="border:1px #fff solid;">
												</li>
												<li class="uk-width-auto mar-pad-r uk-slider-items">
													<img src="img/design/banner1.jpg" alt="" class="mar-pad-r uk-width-small uk-height-small" style="border:1px #fff solid;">
												</li>
											</ul>
										</div>
									</div>
									<ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>
								</div>
							</div>
						</div>
						<div class="uk-width-1-2 mar-pad-r">
							<div class="mar-pad-r" style="background:#6c6c6c;position:relative; z-index:1;width:50vw;margin-left:-21%; margin-top:18%;padding:45px 0">

								<div class="uk-width-1-1 mar-pad-r padding-10">
									<div class="uk-width-1-1  mar-pad-r uk-grid-small" uk-grid style="">
										<div class="uk-width-1-4 mar-pad-r"></div>
										<div class="uk-width-3-4 uk-margin-remove uk-padding">
											<div class="pad-15">
												<h1 class="uk-text-center space4 mar-pad-r txt-30 blanco"> SOFÁS </h1>
												<div class="pad-5 uk-flex uk-flex-center uk-flex-middle">
													<hr class="mar-pad-r hr-4-b">
												</div>
												<h1 class="uk-text-center mar-pad-r txt-20 blanco bold500"> SOFÁS DE 3 PIEZAS</h1>
											</div>
											<div class="bold500 mar-pad-r txt-12 blanco pad-15">
												Lorem Ipsum has been the industry's standard dummy test ever since th 1500s when an unkwnow printer took a gallery of type and scrambied it to Lorem Ipsum has been the industry's standard dummy test
												ever since th 1500s when an unkwnow printer took a gallery of type and scrambied it to.
												Lorem Ipsum has been the industry's standard dummy test ever since th 1500s when an unkwnow printer took a gallery of type and scrambied it to Lorem Ipsum has been the industry's standard dummy test
												ever since th 1500s when an unkwnow printer took a gallery of type and scrambied it to
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
				<!--  -->
				<!-- /* (body) CONTENIDO DE LA VISTA */ -->
			</div>
			@include('layouts.partials_front.social-right')
		</section>

		@include('layouts.partials_front.footer')

		<div id="cotizacion-fixed" class="uk-position-top uk-height-viewport uk-hidden">
			<div>
				<a class="" href="http://localhost/gropiusui/787811585_revisar_orden"><img src="img/design/checkout.png" id="cotizacion-fixed-img"></a>
			</div>
		</div>

		@include('layouts.partials_front.whatsapp')
	</div>

	<div id="spinnermodal" class="uk-modal-full" uk-modal>
		<div class="uk-modal-dialog uk-flex uk-flex-center uk-flex-middle uk-height-viewport">
			<div>
				<div class="claro" uk-spinner="ratio: 5">
				</div>
			</div>
		</div>
	</div>
</section>

@endsection
@section('jsLibExtras2')

@endsection
@section('jqueryExtra')

@endsection
