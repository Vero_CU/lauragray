@extends('layouts.front')

@section('cssExtras')

@endsection
@section('jsLibExtras')

@endsection
@section('styleExtras')

@endsection
@section('content')
	{{-- {{$subastas}} --}}

	<div class="uk-container uk-container-expand uk-margin-remove uk-padding">
		<div class="uk-height-1-1 uk-width-1-1 uk-flex uk-flex-center uk-flex-middle uk-background-cover uk-light height-560" data-src="img/design/banner1.jpg" uk-img style="z-index:1"> </div>
	</div>
	<div class="uk-container uk-container-expand uk-margin-remove">
		<div class="uk-width-1-1 mar-pad-r uk-grid-small" uk-grid>
			<div class="uk-width-1-3@m mar-pad-r border:solid 4px #fff; uk-flex uk-flex-center uk-flex-middle">
				<div>
					<div class="uk-width-1-1 space4 txt-30 uk-flex uk-flex-center"> SUBASTAS </div>
					<div class="uk-width-1-1 space4 txt-12 uk-flex uk-flex-center"> ANTERIORES </div>
					<div class="uk-width-1-1 uk-padding-small">
						<div class="uk-card uk-card-default">
							<div class="padding-10">
								<div class="uK-width-1-1 uk-card-media-top">
									<div class="uk-margin-remove uk-padding-small uk-flex uk-flex-center uk-flex-middle">
										<hr class="mar-pad-r hr-10" style="margin-top:20px!important;">
									</div>
									<div class="gris bold500 uk-margin-remove uk-padding-small txt-14 uk-text-center">
										Lorem Ipsum has been the industry's standard dummy test ever since th 1500s when an unkwnow printer took a gallery of type and scrambied it to
									</div>
									<div class="uk-margin-remove uk-padding-small uk-flex uk-flex-center uk-flex-middle">
										<hr class="mar-pad-r hr-70">
									</div>
									<div class="uk-margin-remove uk-padding-remove uk-flex uk-flex-center uk-flex-middle">
										<span class="mar-pad-r" uk-icon="icon:chevron-down; ratio: 2"></span>
									</div>
								</div>
								<div class="uK-width-1-1 mar-pad-r txt-14 negro line uk-text-center">
									<div class="uk-width-1-1 mar-pad-r">
										<div class="uk-width-1-1 mar-pad-r" uk-slider>
											<div class="uk-width-1-1 mar-pad-r uk-position-relative">
												<div class="uk-width-1-1 mar-pad-ruk-slider-container uk-light">
													<ul class="uk-width-1-1 mar-pad-r uk-slider-items " style="">
														<li class="uk-width-1-3 mar-pad-r uk-slider-items">
															<div class="uk-width-1-1 mar-pad-r uk-flex uk-flex-center uk-flex-middle" style="height:74px; border:solid 1px #fff">
																<img src="img/design/banner1.jpg" alt="" class="mar-pad-r" style="max-height:74px">
															</div>
														</li>
														<li class="uk-width-1-3 mar-pad-r uk-slider-items">
															<div class="uk-width-1-1 mar-pad-r uk-flex uk-flex-center uk-flex-middle" style="height:74px; border:solid 1px #fff">
																<img src="img/design/banner1.jpg" alt="" class="mar-pad-r" style="max-height:74px">
															</div>
														</li>
														<li class="uk-width-1-3 mar-pad-r uk-slider-items">
															<div class="uk-width-1-1 mar-pad-r uk-flex uk-flex-center uk-flex-middle" style="height:74px; border:solid 1px #fff">
																<img src="img/design/banner1.jpg" alt="" class="mar-pad-r" style="max-height:74px">
															</div>
														</li>
														<li class="uk-width-1-3 mar-pad-r uk-slider-items">
															<div class="uk-width-1-1 mar-pad-r uk-flex uk-flex-center uk-flex-middle" style="height:74px; border:solid 1px #fff">
																<img src="img/design/banner1.jpg" alt="" class="mar-pad-r" style="max-height:74px">
															</div>
														</li>
														<li class="uk-width-1-3 mar-pad-r uk-slider-items">
															<div class="uk-width-1-1 mar-pad-r uk-flex uk-flex-center uk-flex-middle" style="height:74px; border:solid 1px #fff">
																<img src="img/design/banner1.jpg" alt="" class="mar-pad-r" style="max-height:74px">
															</div>
														</li>
														<li class="uk-width-1-3 mar-pad-r uk-slider-items">
															<div class="uk-width-1-1 mar-pad-r uk-flex uk-flex-center uk-flex-middle" style="height:74px; border:solid 1px #fff">
																<img src="img/design/banner1.jpg" alt="" class="mar-pad-r" style="max-height:74px">
															</div>
														</li>
													</ul>
												</div>
											</div>
											<ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="uk-width-2-3 uk-margin-remove uk-padding-remove">
				<div class="uk-width-1-1 mar-pad-r">
					<div class="uk-width-1-1 mar-pad-r" uk-slider>
						<div class="uk-width-1-1 mar-pad-r uk-position-relative">
							<div class="uk-width-1-1 mar-pad-ruk-slider-container uk-light">
								<ul class="uk-width-1-1 mar-pad-r uk-slider-items " style="">
									@foreach ($subastas as $sub)
										@if ($loop->index % 3 == 0 )
									<li class="uk-width-1-1 mar-pad-r uk-slider-items">
										<div class="uk-width-1-1 mar-pad-r uk-grid-small" uk-grid>
										@endif
										<div class="uk-width-1-2 padding-10">
											<a class="mar-pad-r uk-link-reset" href="{{ route('front.subasta',$sub->id) }}">
												<div class="uk-card uk-card-default">
													<div class="padding-10">
														<div class="uk-card-media-top">
															<img src="img/photos/subastas/{{$sub->photo}}" alt="">
														</div>
														<div class="uk-padding-small txt-14 negro line uk-text-center">
															<div class="txt-14 bold500 negro pad-5"> {{$sub->titulo_es}} {{$sub->id}}</div>
															<div class="pad-5 txt-card"> Texto descriptivo de el producto, Lorem Ipsum brebe 2 a 3 lineas </div>
															<div class="mar-pad-r uk-flex uk-flex-center uk-flex-middle">
																<hr class="mar-pad-r hr-70">
															</div>
															<div class="gris bold500 pad-5">Inicia desde $ {{$sub->precio_inicial}} </div>
															<div class="txt-14 bold600 negro pad-5 negro space"> PUJAR </div>
														</div>
													</div>
												</div>
											</a>
										</div>
										@if ($loop->index % 3 == 0 )
											</div>
										</li>
										@endif
									@endforeach
								</ul>
							</div>
						</div>
						<ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>
					</div>
				</div>
			</div>
			{{-- <div class="uk-width-2-3 uk-margin-remove uk-padding-remove">
				<div class="uk-width-1-1 mar-pad-r">
					<div class="uk-width-1-1 mar-pad-r" uk-slider>
						<div class="uk-width-1-1 mar-pad-r uk-position-relative">
							<div class="uk-width-1-1 mar-pad-ruk-slider-container uk-light">
								<ul class="uk-width-1-1 mar-pad-r uk-slider-items " style="">
									<li class="uk-width-1-1 mar-pad-r uk-slider-items">
										<div class="uk-width-1-1 mar-pad-r uk-grid-small" uk-grid>
											<div class="uk-width-1-2 padding-10">
												<a class="mar-pad-r uk-link-reset" href="subastas-detalle">
													<div class="uk-card uk-card-default">
														<div class="padding-10">
															<div class="uk-card-media-top">
																<img src="img/design/banner1.jpg" alt="">
															</div>
															<div class="uk-padding-small txt-14 negro line uk-text-center">
																<div class="txt-14 bold500 negro pad-5"> CONSOLA ADARA 0</div>
																<div class="pad-5 txt-card"> Texto descriptivo de el producto, Lorem Ipsum brebe 2 a 3 lineas </div>
																<div class="mar-pad-r uk-flex uk-flex-center uk-flex-middle">
																	<hr class="mar-pad-r hr-70">
																</div>
																<div class="gris bold500 pad-5">Inicia desde $ 45098 </div>
																<div class="txt-14 bold600 negro pad-5 negro space"> PUJAR </div>
															</div>
														</div>
													</div>
												</a>
											</div>
											<div class="uk-width-1-2 padding-10">
												<a class="mar-pad-r uk-link-reset" href="subastas-detalle">
													<div class="uk-card uk-card-default">
														<div class="padding-10">
															<div class="uk-card-media-top">
																<img src="img/design/banner1.jpg" alt="">
															</div>
															<div class="uk-padding-small txt-14 negro line uk-text-center">
																<div class="txt-14 bold500 negro pad-5"> CONSOLA ADARA 1</div>
																<div class="pad-5 txt-card"> Texto descriptivo de el producto, Lorem Ipsum brebe 2 a 3 lineas </div>
																<div class="mar-pad-r uk-flex uk-flex-center uk-flex-middle">
																	<hr class="mar-pad-r hr-70">
																</div>
																<div class="gris bold500 pad-5">Inicia desde $ 45098 </div>
																<div class="txt-14 bold600 negro pad-5 negro space"> PUJAR </div>
															</div>
														</div>
													</div>
												</a>
											</div>
											<div class="uk-width-1-2 padding-10">
												<a class="mar-pad-r uk-link-reset" href="subastas-detalle">
													<div class="uk-card uk-card-default">
														<div class="padding-10">
															<div class="uk-card-media-top">
																<img src="img/design/banner1.jpg" alt="">
															</div>
															<div class="uk-padding-small txt-14 negro line uk-text-center">
																<div class="txt-14 bold500 negro pad-5"> CONSOLA ADARA 2</div>
																<div class="pad-5 txt-card"> Texto descriptivo de el producto, Lorem Ipsum brebe 2 a 3 lineas </div>
																<div class="mar-pad-r uk-flex uk-flex-center uk-flex-middle">
																	<hr class="mar-pad-r hr-70">
																</div>
																<div class="gris bold500 pad-5">Inicia desde $ 45098 </div>
																<div class="txt-14 bold600 negro pad-5 negro space"> PUJAR </div>
															</div>
														</div>
													</div>
												</a>
											</div>
											<div class="uk-width-1-2 padding-10">
												<a class="mar-pad-r uk-link-reset" href="subastas-detalle">
													<div class="uk-card uk-card-default">
														<div class="padding-10">
															<div class="uk-card-media-top">
																<img src="img/design/banner1.jpg" alt="">
															</div>
															<div class="uk-padding-small txt-14 negro line uk-text-center">
																<div class="txt-14 bold500 negro pad-5"> CONSOLA ADARA 3</div>
																<div class="pad-5 txt-card"> Texto descriptivo de el producto, Lorem Ipsum brebe 2 a 3 lineas </div>
																<div class="mar-pad-r uk-flex uk-flex-center uk-flex-middle">
																	<hr class="mar-pad-r hr-70">
																</div>
																<div class="gris bold500 pad-5">Inicia desde $ 45098 </div>
																<div class="txt-14 bold600 negro pad-5 negro space"> PUJAR </div>
															</div>
														</div>
													</div>
												</a>
											</div>
										</div>
									</li>
									<li class="uk-width-1-1 mar-pad-r uk-slider-items">
										<div class="uk-width-1-1 mar-pad-r uk-grid-small" uk-grid>
											<div class="uk-width-1-2 padding-10">
												<a class="mar-pad-r uk-link-reset" href="subastas-detalle">
													<div class="uk-card uk-card-default">
														<div class="padding-10">
															<div class="uk-card-media-top">
																<img src="img/design/banner1.jpg" alt="">
															</div>
															<div class="uk-padding-small txt-14 negro line uk-text-center">
																<div class="txt-14 bold500 negro pad-5"> CONSOLA ADARA 1</div>
																<div class="pad-5 txt-card"> Texto descriptivo de el producto, Lorem Ipsum brebe 2 a 3 lineas </div>
																<div class="mar-pad-r uk-flex uk-flex-center uk-flex-middle">
																	<hr class="mar-pad-r hr-70">
																</div>
																<div class="gris bold500 pad-5">Inicia desde $ 45098 </div>
																<div class="txt-14 bold600 negro pad-5 negro space"> PUJAR </div>
															</div>
														</div>
													</div>
												</a>
											</div>
											<div class="uk-width-1-2 padding-10">
												<a class="mar-pad-r uk-link-reset" href="subastas-detalle">
													<div class="uk-card uk-card-default">
														<div class="padding-10">
															<div class="uk-card-media-top">
																<img src="img/design/banner1.jpg" alt="">
															</div>
															<div class="uk-padding-small txt-14 negro line uk-text-center">
																<div class="txt-14 bold500 negro pad-5"> CONSOLA ADARA 2</div>
																<div class="pad-5 txt-card"> Texto descriptivo de el producto, Lorem Ipsum brebe 2 a 3 lineas </div>
																<div class="mar-pad-r uk-flex uk-flex-center uk-flex-middle">
																	<hr class="mar-pad-r hr-70">
																</div>
																<div class="gris bold500 pad-5">Inicia desde $ 45098 </div>
																<div class="txt-14 bold600 negro pad-5 negro space"> PUJAR </div>
															</div>
														</div>
													</div>
												</a>
											</div>
											<div class="uk-width-1-2 padding-10">
												<a class="mar-pad-r uk-link-reset" href="subastas-detalle">
													<div class="uk-card uk-card-default">
														<div class="padding-10">
															<div class="uk-card-media-top">
																<img src="img/design/banner1.jpg" alt="">
															</div>
															<div class="uk-padding-small txt-14 negro line uk-text-center">
																<div class="txt-14 bold500 negro pad-5"> CONSOLA ADARA 3</div>
																<div class="pad-5 txt-card"> Texto descriptivo de el producto, Lorem Ipsum brebe 2 a 3 lineas </div>
																<div class="mar-pad-r uk-flex uk-flex-center uk-flex-middle">
																	<hr class="mar-pad-r hr-70">
																</div>
																<div class="gris bold500 pad-5">Inicia desde $ 45098 </div>
																<div class="txt-14 bold600 negro pad-5 negro space"> PUJAR </div>
															</div>
														</div>
													</div>
												</a>
											</div>
											<div class="uk-width-1-2 padding-10">
												<a class="mar-pad-r uk-link-reset" href="subastas-detalle">
													<div class="uk-card uk-card-default">
														<div class="padding-10">
															<div class="uk-card-media-top">
																<img src="img/design/banner1.jpg" alt="">
															</div>
															<div class="uk-padding-small txt-14 negro line uk-text-center">
																<div class="txt-14 bold500 negro pad-5"> CONSOLA ADARA 4</div>
																<div class="pad-5 txt-card"> Texto descriptivo de el producto, Lorem Ipsum brebe 2 a 3 lineas </div>
																<div class="mar-pad-r uk-flex uk-flex-center uk-flex-middle">
																	<hr class="mar-pad-r hr-70">
																</div>
																<div class="gris bold500 pad-5">Inicia desde $ 45098 </div>
																<div class="txt-14 bold600 negro pad-5 negro space"> PUJAR </div>
															</div>
														</div>
													</div>
												</a>
											</div>
										</div>
									</li>
									<li class="uk-width-1-1 mar-pad-r uk-slider-items">
										<div class="uk-width-1-1 mar-pad-r uk-grid-small" uk-grid>
											<div class="uk-width-1-2 padding-10">
												<a class="mar-pad-r uk-link-reset" href="subastas-detalle">
													<div class="uk-card uk-card-default">
														<div class="padding-10">
															<div class="uk-card-media-top">
																<img src="img/design/banner1.jpg" alt="">
															</div>
															<div class="uk-padding-small txt-14 negro line uk-text-center">
																<div class="txt-14 bold500 negro pad-5"> CONSOLA ADARA 2</div>
																<div class="pad-5 txt-card"> Texto descriptivo de el producto, Lorem Ipsum brebe 2 a 3 lineas </div>
																<div class="mar-pad-r uk-flex uk-flex-center uk-flex-middle">
																	<hr class="mar-pad-r hr-70">
																</div>
																<div class="gris bold500 pad-5">Inicia desde $ 45098 </div>
																<div class="txt-14 bold600 negro pad-5 negro space"> PUJAR </div>
															</div>
														</div>
													</div>
												</a>
											</div>
											<div class="uk-width-1-2 padding-10">
												<a class="mar-pad-r uk-link-reset" href="subastas-detalle">
													<div class="uk-card uk-card-default">
														<div class="padding-10">
															<div class="uk-card-media-top">
																<img src="img/design/banner1.jpg" alt="">
															</div>
															<div class="uk-padding-small txt-14 negro line uk-text-center">
																<div class="txt-14 bold500 negro pad-5"> CONSOLA ADARA 3</div>
																<div class="pad-5 txt-card"> Texto descriptivo de el producto, Lorem Ipsum brebe 2 a 3 lineas </div>
																<div class="mar-pad-r uk-flex uk-flex-center uk-flex-middle">
																	<hr class="mar-pad-r hr-70">
																</div>
																<div class="gris bold500 pad-5">Inicia desde $ 45098 </div>
																<div class="txt-14 bold600 negro pad-5 negro space"> PUJAR </div>
															</div>
														</div>
													</div>
												</a>
											</div>
											<div class="uk-width-1-2 padding-10">
												<a class="mar-pad-r uk-link-reset" href="subastas-detalle">
													<div class="uk-card uk-card-default">
														<div class="padding-10">
															<div class="uk-card-media-top">
																<img src="img/design/banner1.jpg" alt="">
															</div>
															<div class="uk-padding-small txt-14 negro line uk-text-center">
																<div class="txt-14 bold500 negro pad-5"> CONSOLA ADARA 4</div>
																<div class="pad-5 txt-card"> Texto descriptivo de el producto, Lorem Ipsum brebe 2 a 3 lineas </div>
																<div class="mar-pad-r uk-flex uk-flex-center uk-flex-middle">
																	<hr class="mar-pad-r hr-70">
																</div>
																<div class="gris bold500 pad-5">Inicia desde $ 45098 </div>
																<div class="txt-14 bold600 negro pad-5 negro space"> PUJAR </div>
															</div>
														</div>
													</div>
												</a>
											</div>
											<div class="uk-width-1-2 padding-10">
												<a class="mar-pad-r uk-link-reset" href="subastas-detalle">
													<div class="uk-card uk-card-default">
														<div class="padding-10">
															<div class="uk-card-media-top">
																<img src="img/design/banner1.jpg" alt="">
															</div>
															<div class="uk-padding-small txt-14 negro line uk-text-center">
																<div class="txt-14 bold500 negro pad-5"> CONSOLA ADARA 5</div>
																<div class="pad-5 txt-card"> Texto descriptivo de el producto, Lorem Ipsum brebe 2 a 3 lineas </div>
																<div class="mar-pad-r uk-flex uk-flex-center uk-flex-middle">
																	<hr class="mar-pad-r hr-70">
																</div>
																<div class="gris bold500 pad-5">Inicia desde $ 45098 </div>
																<div class="txt-14 bold600 negro pad-5 negro space"> PUJAR </div>
															</div>
														</div>
													</div>
												</a>
											</div>
										</div>
									</li>
								</ul>
							</div>
						</div>
						<ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>
					</div>
				</div>
			</div> --}}
		</div>
	</div>
@endsection
@section('jsLibExtras2')
<script src="{{asset('js/modules/jquery-countdown/jquery.countdown.js')}}" charset="utf-8"></script>
@endsection
@section('jqueryExtra')
<script type="text/javascript">
	$(document).ready(function() {
		// $('#clock').countdown('', function(event) {
		// 	$(this).html(event.strftime('TIEMPO RESTANTE: %D dias %H horas %M:%S'));
		// });
		$('#clock').countdown('').on('update.countdown', function(event) {
			var format = '%H horas %M:%S';
			if(event.offset.totalDays > 0) {
				format = '%-d dia%!d ' + format;
			}
			if(event.offset.weeks > 0) {
				format = '%-w semana%!w ' + format;
			}

			$(this).html(event.strftime('TIEMPO RESTANTE: '+format));
			})
			.on('finish.countdown', function(event) {
			$(this).html('This offer has expired!')
			.parent().addClass('disabled');
			$('#subsub').attr('disabled','disabled');
			// $('#subsub').addClass('uk-button-secondary');
			$('#subsub').addClass('uk-button-danger');
			$('#puja').attr('disabled','disabled');
			$('#puja').val('');
			// $('#subsub').addClass('disabled');
			// $('#puja').addClass('disabled');
		});
	});
</script>
@endsection
