<!DOCTYPE html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="{{ str_replace('_', '-', app()->getLocale()) }}"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="{{ str_replace('_', '-', app()->getLocale()) }}"> <!--<![endif]-->
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<meta name="msapplication-tap-highlight" content="no"/>

		<link rel="icon" type="image/png" href="{{asset('assets/img/logo-wozial.ico')}}" sizes="16x16">
		<link rel="icon" type="image/png" href="{{asset('assets/img/logo-wozial.ico')}}" sizes="32x32">

		<title>Administración</title>

		<!-- uikit -->
		{{-- <link rel="stylesheet" href="{{ asset('bower_components/uikit/css/uikit.almost-flat.min.css')}}" media="all"> --}}
		<link rel="stylesheet" href="{{ asset('bower_components/uikit/dist/css/uikit.min.css')}}" media="all">
		<!-- flag icons -->
		<link rel="stylesheet" href="{{ asset('assets/icons/flags/flags.min.css')}}" media="all">
		<!-- altair admin -->
		<link rel="stylesheet" href="{{ asset('assets/css/main.min.css')}}" media="all">

		<!-- matchMedia polyfill for testing media queries in JS -->
		<!--[if lte IE 9]>
				<script type="text/javascript" src="{{ asset('bower_components/matchMedia/matchMedia.js')}}"></script>
				<script type="text/javascript" src="{{ asset('bower_components/matchMedia/matchMedia.addListener.js')}}"></script>
		<![endif]-->
		<script src="https://kit.fontawesome.com/910783a909.js" crossorigin="anonymous"></script>
</head>
<body class=" sidebar_main_open sidebar_main_swipe">
		@include('layouts.partials_ad.header')
		@include('layouts.partials_ad.sidebar')

		<div id="page_content">
				<div id="page_content_inner">
					@yield('content')
				</div>
		</div>

		<!-- google web fonts -->
		<script>
				WebFontConfig = {
						google: {
								families: [
										'Source+Code+Pro:400,700:latin',
										'Roboto:400,300,500,700,400italic:latin'
								]
						}
				};
				(function() {
						var wf = document.createElement('script');
						wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
						'://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
						wf.type = 'text/javascript';
						wf.async = 'true';
						var s = document.getElementsByTagName('script')[0];
						s.parentNode.insertBefore(wf, s);
				})();
		</script>

		<!-- common functions -->
		<script src="{{ asset('assets/js/common.min.js')}}"></script>
		<!-- uikit functions -->
		<script src="{{ asset('assets/js/uikit_custom.min.js')}}"></script>
		<!-- altair common functions/helpers -->
		<script src="{{ asset('assets/js/altair_admin_common.min.js')}}"></script>

		<script>
				$(function() {
						// enable hires images
						altair_helpers.retina_images();
						// fastClick (touch devices)
						if(Modernizr.touch) {
								FastClick.attach(document.body);
						}
				});
		</script>
</body>
</html>
