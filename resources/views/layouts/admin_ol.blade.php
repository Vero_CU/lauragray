<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>Administración</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="shortcut icon" href="{{asset('img/design/logo-wozial.ico')}}">

	<!-- Font Awsome -->
	<script src="https://kit.fontawesome.com/910783a909.js" crossorigin="anonymous"></script>

	<!-- UIkit CSS -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@ {{env('UIKITVERSION')}}/dist/css/uikit.min.css" />

	<!-- jQuery es neceario -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

	<!-- UIkit JS -->
	<script src="https://cdn.jsdelivr.net/npm/uikit@ {{env('UIKITVERSION')}}/dist/js/uikit.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/uikit@ {{env('UIKITVERSION')}}/dist/js/uikit-icons.min.js"></script>


	<!-- CSS Personalizados -->
	<link rel="stylesheet" href="{{asset('css/admin.css')}}">

</head>
<body>
	<div id="admin" class="uk-offcanvas-content">
		<div id="adminmenu">
			<div id="menudisplay" class="uk-height-viewport" uk-sticky>
				<div class="uk-visible@l">
					<div>
						<div class="padding-10">
							<a href="{{ url('/admin')}}" target="_blank"><img src="{{ asset('img/design/logo-wozial.png')}}"></a>
						</div>
					</div>
					<div>
						<nav>
							<ul class="uk-nav-default uk-nav-parent-icon uk-text-uppercase" id="menu-large" uk-nav>
								'.$menu.'
							</ul>
						</nav>
					</div>
				</div>
				<div class="uk-position-bottom uk-text-muted">
					<div class="text-v" id="user-bar">
						{{ auth::user()->user}}
					</div>
					<div style="padding:20px 0 10px 15px;">
						<i uk-icon="icon:user;ratio:1.2;"></i>
					</div>
					<div style="padding:0 0 30px 7px;">
						<a class="uk-icon-button uk-button-danger" href="{{ route('admin.logout') }}"
							 onclick="event.preventDefault();
														 document.getElementById('logout-form').submit();">
								<span uk-icon="icon:unlock;"></span>
						</a>

						<form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
								@csrf
						</form>
					</div>
				</div>
			</div>
			<div id="menu-movil" uk-offcanvas="mode: push; overlay: true">
				<div class="uk-offcanvas-bar uk-flex uk-flex-column">
					<button class="uk-offcanvas-close" type="button" uk-close></button>
					<ul class="uk-nav uk-nav-primary uk-nav-parent-icon uk-nav-center uk-margin-auto-vertical menu-movil uk-text-uppercase" uk-nav>
						'.$menuMovil.'
					</ul>
					<div class="uk-text-center">
						<a href="index.php?logout=1" class="uk-icon-button uk-button-danger" uk-icon="icon:unlock;"></a>
					</div>
				</div>
			</div>
		</div>
		<div id="admincuerpo">
			<div class="uk-container uk-container-expand">
				<div class="uk-width-1-1 uk-hidden@l">
					<a href="#menu-movil" uk-toggle class="uk-button uk-button-white"><i uk-icon="icon:menu;ratio:1.4;"></i> &nbsp; MENÚ</a>
					<span class="uk-float-right uk-text-muted"><i uk-icon="icon:user;ratio:1.1;"></i> &nbsp; {{ auth::user()->user}}<span>
				</div>
				<div uk-grid>
					@yield('content')
				</div>
			</div>
		</div>
	</div>
	<!-- JQUERY UI -->
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

	<!-- Upload Image -->
	<link href="{{ asset('vendor/upload-file/css/uploadfile.css')}}" rel="stylesheet">
	<script src="{{ asset('vendor/upload-file/js/jquery.uploadfile.js')}}"></script>

	<!-- Editor de texto -->
	<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>

	<!-- Chosen -->
	<link  href="{{ asset('vendor/chosen/chosen.admin.css')}}" rel="stylesheet">
	<script src="{{ asset('vendor/chosen/chosen.jquery.js')}}" type="text/javascript"></script>
	<script src="{{ asset('vendor/chosen/docsupport/prism.js')}}" type="text/javascript" charset="utf-8"></script>

	<!-- Scripts Personalizados -->
	<script src="{{ asset('js/admin.js')}}"></script>

	<script>
	@yield('jsExtra')
	</script>
</body>
</html>
