<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Admin') }}</title>

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
	<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
	{{-- <link rel="stylesheet" href="{{asset('css/mdb.min.css')}}"> --}}
	<link rel="stylesheet" href="{{asset('css/mdb.css')}}">
	@yield('cssExtras')

	@yield('jsLibExtras')

	@yield('styleExtras')
</head>

<body class="fixed-sn black-skin">

	@include('layouts.partials_ad.header')

	<main >
		<div class="container-fluid">
			@yield('content')
		</div>
	</main>

	{{-- <footer class="page-footer pt-0 mt-5 rgba-stylish-light">
		<div class="footer-copyright py-3 text-center">
			<div class="container-fluid">
				© 2019 Copyright: <a href="https://mdbootstrap.com" target="_blank"> MDBootstrap.com </a>
			</div>
		</div>
	</footer> --}}

	<script src="{{asset('js/jquery-3.4.1.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/popper.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/bootstrap.js')}}"></script>
	{{-- <script type="text/javascript" src="{{asset('js/mdb.min.js')}}"></script> --}}
	<script type="text/javascript" src="{{asset('js/mdb.js')}}"></script>
	@yield('jsLibExtras2')
	<script type="text/javascript">
		$(".button-collapse").sideNav();
		var sideNavScrollbar = document.querySelector('.custom-scrollbar');
		Ps.initialize(sideNavScrollbar);
	</script>
	@yield('scriptExtras')

</body>

</html>
