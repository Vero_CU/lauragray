<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	{{-- social --}}
	{{-- <meta name="description" content="Software de desarrollo Wozial" />
	<meta property="fb:app_id" content="298662384195873" />
	<link rel="image_src" href="http://localhost/gropiusui/img/design/logo-og.jpg" />

	<meta property="og:type" content="website" />
	<meta property="og:title" content="SDK" />
	<meta property="og:description" content="Software de desarrollo Wozial" />
	<meta property="og:url" content="http://localhost/gropiusui/tienda-detalle" />
	<meta property="og:image" content="http://localhost/gropiusui/img/design/logo-og.jpg" />

	<meta itemprop="name" content="SDK" />
	<meta itemprop="description" content="Software de desarrollo Wozial" />
	<meta itemprop="url" content="http://localhost/gropiusui/tienda-detalle" />
	<meta itemprop="thumbnailUrl" content="http://localhost/gropiusui/img/design/logo-og.jpg" />
	<meta itemprop="image" content="http://localhost/gropiusui/img/design/logo-og.jpg" />

	<meta name="twitter:title" content="SDK" />
	<meta name="twitter:description" content="Software de desarrollo Wozial" />
	<meta name="twitter:url" content="http://localhost/gropiusui/tienda-detalle" />
	<meta name="twitter:image" content="http://localhost/gropiusui/img/design/logo-og.jpg" />
	<meta name="twitter:card" content="summary" /> --}}
	{{-- end social --}}

	<title>Document</title>

	<link rel="icon"            href="{{asset('img/design/favicon.ico')}}" type="image/x-icon">
	<link rel="shortcut icon"   href="{{asset('img/design/favicon.ico')}}" type="image/x-icon">
	<link rel="stylesheet"      href="{{asset('css/uikit/uikit.min.css')}}" />
	<link rel="stylesheet/less" href="{{asset('css/uikit/general.less')}}" >
	<link rel="stylesheet"      href="https://fonts.googleapis.com/css?family=Lato:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;900&display=swap" rel="stylesheet">

	@yield('cssExtras')
		@yield('jsLibExtras')
		@yield('styleExtras')

</head>
<body>
{{-- <body class="mar-pad-r"> --}}

	@yield('content')


	<script src="{{asset('js/jquery-3.4.1.min.js')}}" type="text/javascript"></script>
	<script src="{{asset('js/uikit/uikit.min.js')}}" type="text/javascript"></script>
	<script src="{{asset('js/uikit/uikit-icons.min.js')}}" type="text/javascript"></script>
	<script src="https://kit.fontawesome.com/910783a909.js" crossorigin="anonymous"></script>
	<script src="{{asset('js/uikit/less.min.js')}}" type="text/javascript"></script>
	<script src="{{asset('js/uikit/general.js')}}"></script>
	{!! Toastr::message() !!}
	@yield('jsLibExtras2')

	<script type="text/javascript">
		$(document).ready(function() {
			setTimeout(function(){
				$("#whatsapp-plugin").addClass("uk-animation-slide-bottom-small");
				$("#whatsapp-plugin").removeClass("uk-hidden");
			},1000);

			setTimeout(function(){
				$("#whats-body-1").addClass("uk-hidden");
				$("#whats-body-2").removeClass("uk-hidden");
			},6000);

			// $("#whats-close").click(function(){
			// 	$("#whatsapp-plugin").addClass("uk-hidden");
			// 	$("#whats-show").removeClass("uk-hidden");
			// 	$.ajax({
			// 		method: "POST",
			// 		url: "includes/acciones.php",
			// 		data: {
			// 			whatsappHiden: 1
			// 		}
			// 	})
			// 	.done(function( msg ) {
			// 		console.log(msg);
			// 	});
			// });

			// $("#whats-show").click(function(){
			// 	$("#whatsapp-plugin").removeClass("uk-hidden");
			// 	$("#whats-show").addClass("uk-hidden");
			// 	$("#whats-body-1").addClass("uk-hidden");
			// 	$("#whats-body-2").removeClass("uk-hidden");
			// 	$.ajax({
			// 		method: "POST",
			// 		url: "includes/acciones.php",
			// 		data: {
			// 			whatsappShow: 1
			// 		}
			// 	})
			// 	.done(function( msg ) {
			// 		console.log(msg);
			// 	});
			// });

		});
	</script>
	@yield('jqueryExtra')
</body>
</html>
