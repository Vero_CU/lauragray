@extends('layouts.admin')

@section('cssExtras')
@endsection

@section('jsLibExtras')
@endsection

@section('content')
	<div class="row mb-4 px-2">
		<a href="{{ route('config.index') }}" class="col disabled col-md-2 btn btn-sm grey darken-2 text-white mr-auto"><i class="fa fa-reply"></i> Regresar</a>
	</div>

	<div class="col-12 px-0 mx-auto">
		<div class="card">
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-striped table-hover table-sm">
						<thead>
							<th style="width: 3rem;">id</th>
							<th>Nombre</th>
							<th>Apellidos</th>
							<th>Email</th>
							<th>Pedidos</th>
							<th class="text-center" style="width: 10rem;">Ops</th>
						</thead>
						<tbody>
							@foreach ($clientes as $cl)
								<tr>
									<td>{{$cl->id}}</td>
									<td>{{$cl->name}}</td>
									<td>{{$cl->lastname}}</td>
									<td>{{$cl->email}}</td>
									<!--HADCODEADO HAY QUE CONSULTAR LAS REALES-->
									<td>5</td>
									<td>
										<div class="dropdown text-right">
											<button class="btn btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												Accciones
											</button>
											<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
												<a class="dropdown-item" href="{{route('clientes.show', $cl->id)}}">Ver más</a>
												<!--a class="dropdown-item" href="">Editar</a-->
												<a class="dropdown-item" href="#">Eliminar</a>
											</div>
										</div>
										{{-- <div class="text-right">
											<div class="btn-group btn-group-sm" role="group">
												<a href="" class="btn btn-sm btn-secondary m-0"><i class="fas fa-search-plus"></i></a>
												<button type="button" class="btn btn-sm btn-danger m-0" data-toggle="modal" data-target="#frameModalDel" data-id=""><i class="fas fa-trash-alt"></i></button>
											</div>
										</div> --}}
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

@endsection
@section('jqueryExtra')
@endsection
