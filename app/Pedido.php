<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{

	protected $fillable = [
		'uid','estatus','guia','linkguia','domicilio','factura','cantidad','importe','iva','envio','comprobante','cupon','usuario'
	];

	public function pedidosDetalle(){
		return $this->hasMany('App\PedidoDetalle','pedido');
	}

	public function usuario(){
		return $this->belongsTo('App\User','usuario');
	}

}
