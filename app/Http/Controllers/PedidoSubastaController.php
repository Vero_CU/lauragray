<?php

namespace App\Http\Controllers;

use App\Subasta;
use App\PedidoSubasta;
use Illuminate\Http\Request;

class PedidoSubastaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
			$subastas = '';
			return view('admin.subastas.ordenes',compact('subastas'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
			switch ($request->route) {
				case 'paypal':
				return redirect()->route('paypal.paypalpay',$request->uid);
				break;
				case 'coneckta':
				return redirect()->route('conekta.card',$request->uid);
				break;
			}
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PedidoSubasta  $pedidoSubasta
     * @return \Illuminate\Http\Response
     */
    public function show(PedidoSubasta $pedidoSubasta)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PedidoSubasta  $pedidoSubasta
     * @return \Illuminate\Http\Response
     */
    public function edit(PedidoSubasta $pedidoSubasta)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PedidoSubasta  $pedidoSubasta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PedidoSubasta $pedidoSubasta)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PedidoSubasta  $pedidoSubasta
     * @return \Illuminate\Http\Response
     */
    public function destroy(PedidoSubasta $pedidoSubasta)
    {
        //
    }
}
