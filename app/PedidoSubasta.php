<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PedidoSubasta extends Model {

	protected $fillable = [
		'uid','subasta','estatus','guia','linkguia','domicilio','factura','importe','iva','total','envio','comprobante','cancelado',
	];
}
