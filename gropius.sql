-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 30-01-2021 a las 00:38:19
-- Versión del servidor: 10.3.25-MariaDB-0ubuntu0.20.04.1
-- Versión de PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `gropius`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` tinyint(4) NOT NULL,
  `menu_display` tinyint(1) DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `admins`
--

INSERT INTO `admins` (`id`, `user`, `email`, `password`, `role`, `menu_display`, `activo`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@wozial.com', '$2y$10$1zm6ifKC5Lnx2TFmlQVrCe9tQFDvOoercnMRow5fx3.5u8aHbF.U2', 1, NULL, 1, 'r1aVa296omrd0PenC3FWKAjAwtdla6jQRnTnvA3yNFbH2uP9SsZ289SlhOrK', '2020-10-14 03:24:37', '2020-10-14 03:24:37');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carrusels`
--

CREATE TABLE `carrusels` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `titulo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subtitulo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orden` int(11) DEFAULT 99
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `carrusels`
--

INSERT INTO `carrusels` (`id`, `titulo`, `subtitulo`, `image`, `url`, `video`, `orden`) VALUES
(4, NULL, NULL, NULL, NULL, 'https://www.youtube.com/watch?v=uJbrTgUL0Dk', 3),
(6, NULL, NULL, 'BqDeZhTiYwyFUtjrpGFfELhzwJKCiv.jpeg', NULL, NULL, 99);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` int(11) NOT NULL DEFAULT 0,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  `orden` int(11) NOT NULL DEFAULT 99
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id`, `nombre`, `parent`, `activo`, `orden`) VALUES
(1, 'sofas', 0, 1, 99),
(2, 'comedor', 0, 1, 99),
(3, 'sofas 3', 1, 1, 99),
(4, 'sala', 0, 1, 99),
(5, 'Exteriores', 0, 1, 99),
(6, 'Jardin', 5, 1, 99),
(7, 'Recamara', 0, 1, 99),
(8, 'pau', 0, 1, 99),
(9, 'pau1', 8, 1, 99);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categ_tamanos`
--

CREATE TABLE `categ_tamanos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sizeName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `orden` int(11) NOT NULL DEFAULT 99,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `categ_tamanos`
--

INSERT INTO `categ_tamanos` (`id`, `sizeName`, `slug`, `orden`, `created_at`, `updated_at`) VALUES
(1, 'Caballaje', 'caballaje', 99, '2020-12-24 01:25:48', '2020-12-24 01:25:48');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `colors`
--

CREATE TABLE `colors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hexa` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `textura` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `BoolTexture` tinyint(1) NOT NULL DEFAULT 0,
  `orden` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `colors`
--

INSERT INTO `colors` (`id`, `name`, `hexa`, `textura`, `BoolTexture`, `orden`) VALUES
(1, 'Negro', '#000000', NULL, 0, 0),
(2, 'akatsuki', NULL, 'uXMvxvHi34hLc0BpYMm1VR75JDTbsl.jpeg', 1, 1),
(3, 'rombo gris', NULL, 'YJSo1joKniRQTzUQ7JtwS1VH3N6APZ.jpeg', 1, 3),
(4, 'diagonal amarillo', NULL, '4eoXItkmtuJG28Sy0kCPwWHpy4SLxs.png', 1, 4),
(5, 'rojo', '#ff0000', NULL, 0, 2),
(6, 'amarillo', '#ffdd00', NULL, 0, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuracions`
--

CREATE TABLE `configuracions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prodspag` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sliderhmin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `sliderhmax` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1000',
  `sliderproporcion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slideranim` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slidertextos` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paypalemail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `destinatario` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `destinatario2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remitente` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remitentepass` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remitentehost` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remitenteport` varchar(6) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remitenteseguridad` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telefono` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telefono2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `envio` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `envioglobal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iva` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `incremento` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aboutimg` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banco` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `configuracions`
--

INSERT INTO `configuracions` (`id`, `title`, `description`, `prodspag`, `sliderhmin`, `sliderhmax`, `sliderproporcion`, `slideranim`, `slidertextos`, `paypalemail`, `destinatario`, `destinatario2`, `remitente`, `remitentepass`, `remitentehost`, `remitenteport`, `remitenteseguridad`, `telefono`, `telefono2`, `facebook`, `instagram`, `youtube`, `linkedin`, `envio`, `envioglobal`, `iva`, `incremento`, `about`, `aboutimg`, `banco`, `created_at`, `updated_at`) VALUES
(1, 'Wozial shop', 'Wozial Software Development Lovers', '5', '0', '1000', NULL, NULL, NULL, 'business@wozial.mx', 'desarrollo@wozial.com', NULL, 'contacto@eshot.mx', 'LeGuaGua@ElPerrito', 'mail.eshot.mx', '465', NULL, '3300000000', '3300000000', 'https://www.facebook.com/', 'https://www.instagram.com/', 'https://www.youtube.com/', 'https://www.linkedin.com/', '0', '500', '16', NULL, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas laudantium saepe magni mollitia quibusdam nemo veniam maxime explicabo enim veritatis tenetur accusamus debitis voluptate delectus architecto porro, facilis ipsam ratione iste voluptates, ab voluptatem. Fugit laudantium vitae reprehenderit consequatur eos mollitia minima consectetur! Doloremque dolorum minima incidunt assumenda, laboriosam quo asperiores hic tenetur velit atque quasi quae aliquam perspiciatis nemo harum tempora corrupti possimus pariatur adipisci recusandae fugit, eveniet. Deleniti incidunt porro sit illo totam quisquam voluptatum repudiandae iste, ullam delectus quidem rerum enim, similique explicabo aperiam maxime ut fugit officiis ipsam quia. Libero labore ipsum tenetur, porro, consequatur sequi distinctio minima numquam tempore maiores aliquam, illum incidunt quod impedit repellendus eveniet quae fuga ex.', 'Q1EpPk1qbtRv0GHpoET8dH6w8ayF6X.jpg', 'Banco: Bancomer\nTarjeta: 1234567891012346\nCuenta: 123123132\nClabe: 123456789101112130', '2020-11-20 22:41:38', '2021-01-30 09:35:50');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cupons`
--

CREATE TABLE `cupons` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `codigo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `porcentaje` int(11) DEFAULT NULL,
  `cantidad` double(6,2) DEFAULT NULL,
  `vigencia` date NOT NULL,
  `usos` int(11) DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 0,
  `orden` int(11) NOT NULL DEFAULT 99,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `domicilios`
--

CREATE TABLE `domicilios` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `alias` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `calle` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `numext` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `numint` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entrecalles` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `colonia` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `municipio` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pais` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'Mexico',
  `favorito` tinyint(1) DEFAULT NULL,
  `user` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `elementos`
--

CREATE TABLE `elementos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `elemnto` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `texto` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imagen` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seccion` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `elementos`
--

INSERT INTO `elementos` (`id`, `elemnto`, `texto`, `imagen`, `url`, `seccion`) VALUES
(1, 'bloque1', 'Index Bloque 1 Editable\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Donec dictum vulputate eleifend. Quisque id imperdiet nulla. Morbi ultricies augue quis elit pharetra condimentum. Etiam aliquet lectus ac quam vulputate sollicitudin. Morbi faucibus velit orci, eget convallis tortor sollicitudin in. Vestibulum semper turpis ut condimentum scelerisque. Donec a ante rutrum, scelerisque mi id, venenatis neque. Nulla non condimentum ligula. Morbi rhoncus orci ante. Duis id consectetur nunc. Ut sagittis est vitae varius gravida.', NULL, NULL, 1),
(2, 'bloque2', 'Index Bloque 2 Editable\nMaecenas hendrerit finibus lacinia. Fusce consequat nisi non urna lacinia vestibulum. In id nisi nec eros euismod consectetur id non enim. Nulla vitae nisi id tellus iaculis aliquam. Praesent tristique dapibus mollis. Pellentesque aliquam rhoncus placerat. Donec ac erat bibendum, vulputate nunc id, tempus ipsum. Interdum et malesuada fames ac ante ipsum primis in faucibus.', NULL, NULL, 1),
(3, 'bloque3', 'Index Bloque 3 Editable\nAliquam ac porttitor erat, eget fermentum nisl. Ut cursus non urna vestibulum mollis. Sed dictum purus dolor, id mollis augue interdum at. Curabitur pulvinar at sem id condimentum. Nullam consectetur justo tristique orci finibus, ac bibendum justo varius. Proin feugiat euismod lectus, ut molestie nibh tempor id. Nulla magna justo, porta eget tristique eu, cursus eu odio.', NULL, NULL, 1),
(4, 'bloque1', 'Subastas Bloque 1 Editable\nCras eget massa a massa gravida tempus ac eget justo. In at turpis sed libero vehicula mattis sit amet non tellus. Mauris elementum ipsum nisi, in interdum turpis rhoncus in. In facilisis nunc et felis luctus dapibus. Donec ut sapien libero. Nam ac luctus odio. Donec a elit justo. Nulla luctus tellus nec tortor rhoncus volutpat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Mauris et tincidunt dui, pretium vulputate orci. Ut a dolor elementum nibh mattis consequat sit amet eget mi. Cras tincidunt volutpat urna, vel egestas lectus faucibus nec. Suspendisse rutrum, est id fringilla volutpat, dolor velit posuere mi, at pretium odio mi eu lorem. In non lorem eu dolor luctus dictum. Ut sed consequat ligula. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.', NULL, NULL, 4),
(5, 'bloque-1', 'Contacto Bloque 1 Editable\nMorbi semper vulputate ex, ut finibus lacus. Aliquam erat volutpat. Nullam lobortis consequat consectetur. Duis suscipit nunc vitae consequat condimentum. Vivamus hendrerit leo et eleifend sodales. Proin sed ligula in mi convallis scelerisque. Sed ultrices lacinia ante, id viverra leo elementum eu. Maecenas tincidunt nibh ex, id vestibulum justo lobortis sed. Proin quis velit ac risus consequat ullamcorper. Nunc egestas blandit mauris, cursus aliquet ligula porttitor congue. Donec ac neque ex. Phasellus dignissim, odio tincidunt posuere ornare, magna eros molestie diam, sit amet ornare purus nisi et lacus. Etiam tincidunt eros et lacus aliquam dictum.', NULL, NULL, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `espacioproductos`
--

CREATE TABLE `espacioproductos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `producto` bigint(20) UNSIGNED NOT NULL,
  `espacio` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `espacios`
--

CREATE TABLE `espacios` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `titulo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subtitulo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orden` int(11) NOT NULL DEFAULT 99
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `espacios`
--

INSERT INTO `espacios` (`id`, `titulo`, `subtitulo`, `image`, `orden`) VALUES
(1, NULL, NULL, 'L6qSSMuAg3FPsOnafuervAT2eDqaEG.jpg', 99),
(2, NULL, NULL, 'bj0qfl55qQlIA54XEjDG52AQEw7soG.jpg', 99);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturas`
--

CREATE TABLE `facturas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `rfc` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mail` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `razon_social` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `calle` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `numext` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `numint` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `colonia` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `municipio` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `facturas`
--

INSERT INTO `facturas` (`id`, `rfc`, `mail`, `razon_social`, `calle`, `numext`, `numint`, `colonia`, `cp`, `municipio`, `estado`, `user`) VALUES
(1, 'RULE940202KH8', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Aguascalientes', 1),
(2, 'RULE940202KH8', 'yahir@wozial.com', 'wozial', 'av. Lapizlazuli', '2074', '3', 'bosques de la victoria', '45089', 'Zapopan', 'Jalisco', 2),
(3, 'asdfasdf', 'asdfasdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Jalisco', 5),
(4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `faqs`
--

CREATE TABLE `faqs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pregunta` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `respuesta` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `orden` int(11) NOT NULL DEFAULT 99,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `faqs`
--

INSERT INTO `faqs` (`id`, `pregunta`, `respuesta`, `orden`, `created_at`, `updated_at`) VALUES
(1, '¿Debo usar cubrebocas?', '<div class=\"wgl-accordion_content\" style=\"display: block;\">\r\n<p>El cubreboca es una barrera &uacute;til para evitar que se expulsen las gotas de saliva que potencialmente est&eacute;n contaminadas. El cubreboca se debe usar correctamente, sobre todo en lugares p&uacute;blicos cerrados en los que no se pueda mantener la sana distancia. No te olvides de usar el cubreboca junto con las medidas b&aacute;sicas de prevenci&oacute;n como son la sana distancia (dos metros), lavado frecuente de manos y estornudo de etiqueta.</p>\r\n</div>', 0, '2020-12-17 01:16:08', '2020-12-18 22:54:54'),
(2, '¿Debo aislarme si tuve contacto con un caso confirmado?', '<div class=\"wgl-accordion_content\" style=\"display: block;\">\r\n<div class=\"wgl-accordion_content\" style=\"display: block;\">\r\n<p>El aislamiento es una medida voluntaria: a nadie se le puede obligar, ni siquiera a los casos confirmados. Por fortuna, todos han aceptado la cuarentena para no diseminar la enfermedad.</p>\r\n<p>Hay muchos casos de personas que decidieron ponerse en cuarentena al comprobar que estuvieron cerca de alguien que se enferm&oacute; de COVID-19.</p>\r\n</div>\r\n</div>', 2, '2020-12-17 01:16:10', '2020-12-18 22:55:33'),
(3, 'Soy usuario de Sistema Amigo, ¿qué necesito para contratar un Plan de Renta?', '<div class=\"wgl-accordion_content\" style=\"display: block;\">\r\n<p>Los requisitos para contratar un Plan de Renta Telcel son los siguientes:<br />&bull; Identificaci&oacute;n oficial vigente.<br />&bull; Comprobante de domicilio.<br />&bull; Dep&oacute;sito en garant&iacute;a y/o tarjeta de cr&eacute;dito (a nombre del titular y no puede ser corporativa ni empresarial).<br />&bull; Factura a nombre del titular o endosada, presentando el equipo celular.</p>\r\n<p>Adem&aacute;s, para persona Moral:<br />&bull; Acta Constitutiva de la empresa.<br />&bull; C&eacute;dula del Registro Federal de Contribuyentes.<br />&bull; Cheque nominativo por la 1&ordf; renta de acuerdo con el Plan Tarifario, a nombre de Radiom&oacute;vil DIPSA S.A. de C.V.<br />&bull; Dep&oacute;sito en garant&iacute;a y/o tarjeta de cr&eacute;dito corporativa o empresarial a nombre del representante legal (en caso de no tener referencias).</p>\r\n</div>', 3, '2020-12-17 04:20:09', '2020-12-18 22:54:54');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `galeria_subastas`
--

CREATE TABLE `galeria_subastas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orden` int(11) NOT NULL DEFAULT 99
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(5, '2020_10_13_163806_create_admins_table', 1),
(6, '2020_10_14_181530_create_configuracions_table', 1),
(7, '2020_11_23_155924_create_colors_table', 2),
(8, '2020_12_08_170359_create_carrusels_table', 3),
(9, '2020_12_09_193424_create_politicas_table', 4),
(10, '2020_12_16_000636_create_faqs_table', 5),
(11, '2020_12_18_163712_create_categ_tamanos_table', 6),
(12, '2020_12_18_163750_create_sizes_table', 6),
(18, '2020_12_24_194725_create_cupons_table', 7),
(22, '2021_01_10_233204_create_subastas_table', 8),
(24, '2021_01_11_050541_create_subastas_photos_table', 9),
(25, '2021_01_11_100948_create_pujas_table', 10),
(26, '2021_01_16_102703_create_categorias_table', 11),
(27, '2021_01_16_103723_create_productos_table', 12),
(28, '2021_01_16_135303_create_productos_photos_table', 13),
(30, '2021_01_20_025504_create_producto_versions_table', 14),
(31, '2021_01_20_110254_create_producto_version_photos_table', 14),
(32, '2021_01_20_130231_create_producto_relacions_table', 15),
(33, '2021_01_21_215846_create_facturas_table', 16),
(42, '2021_01_22_192915_create_espacios_table', 17),
(43, '2021_01_22_221443_create_nosotrosgalerias_table', 18),
(44, '2021_01_25_180515_create_seccions_table', 19),
(45, '2021_01_25_180534_create_elementos_table', 19),
(53, '2020_10_12_153249_create_domicilios_table', 20),
(54, '2021_01_25_195631_create_espacioproductos_table', 21),
(55, '2021_01_27_192446_create_pedidos_table', 22),
(56, '2021_01_27_212728_create_pedido_detalles_table', 22),
(57, '2021_01_28_013618_create_sucursals_table', 23),
(58, '2021_01_29_191337_create_galeria_subastas_table', 24),
(59, '2021_01_30_010556_create_newslatters_table', 25);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `newslatters`
--

CREATE TABLE `newslatters` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nombre` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nosotrosgalerias`
--

CREATE TABLE `nosotrosgalerias` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orden` int(11) NOT NULL DEFAULT 99
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `nosotrosgalerias`
--

INSERT INTO `nosotrosgalerias` (`id`, `image`, `url`, `orden`) VALUES
(1, 'hYlcpoheExqRdbKx7afse6PWA4qcHK.jpg', NULL, 99),
(2, 'DNzNCha7VcjEmTgUOEdXsh9knfrG56.jpg', NULL, 99),
(3, '5LtANPL87LAfH0Oj13UMlnm6E2SOxM.jpg', NULL, 99),
(4, 'ISJgvAx3pMZEuCk7iLn4W5ZfQpBYKV.jpg', NULL, 99);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidos`
--

CREATE TABLE `pedidos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estatus` int(11) DEFAULT NULL,
  `guia` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkguia` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `domicilio` bigint(20) UNSIGNED NOT NULL,
  `factura` tinyint(1) DEFAULT NULL,
  `cantidad` int(11) NOT NULL,
  `importe` double(9,2) NOT NULL,
  `envio` double(9,2) DEFAULT NULL,
  `comprobante` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cupon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cancelado` tinyint(1) DEFAULT 0,
  `usuario` bigint(20) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido_detalles`
--

CREATE TABLE `pedido_detalles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` double(9,2) NOT NULL,
  `importe` double(9,2) NOT NULL,
  `pedido` bigint(20) UNSIGNED NOT NULL,
  `producto` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `politicas`
--

CREATE TABLE `politicas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `titulo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `archivo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `politicas`
--

INSERT INTO `politicas` (`id`, `titulo`, `descripcion`, `archivo`, `created_at`, `updated_at`) VALUES
(1, 'Aviso de privacidad', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin id nulla ac libero viverra laoreet. Duis varius scelerisque nunc at feugiat. Sed viverra est non fringilla pellentesque. Sed dictum suscipit tristique. In ultricies neque vel aliquam pharetra. Aliquam magna dolor, accumsan a mi id, commodo consequat purus. Nullam lobortis erat a tempor blandit. Quisque semper turpis in erat cursus, id auctor nisi sollicitudin. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer at blandit lectus. Pellentesque aliquet velit sem, vitae mollis eros tempor vel. Duis id orci in nulla viverra dignissim at a sem. Mauris iaculis nisl nec enim rhoncus iaculis. Curabitur dapibus fringilla quam, sed blandit ipsum accumsan nec. Donec ac elit lobortis purus sagittis convallis quis et est. Praesent vitae sagittis felis, ac sagittis tortor. Cras tortor lectus, molestie consequat ipsum id, efficitur ullamcorper felis. Sed sapien ipsum, rutrum a odio id, gravida ultrices neque. Nullam finibus mi vel ante dignissim auctor. In nec diam in ipsum dictum auctor quis sit amet sapien. Mauris augue enim, volutpat a malesuada id, hendrerit vitae neque. Aliquam erat volutpat. Etiam ut finibus neque. Nulla et finibus felis. Etiam vestibulum orci id nisl iaculis sodales. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce fermentum rutrum mi non faucibus. Nulla molestie urna eu orci malesuada dictum. Integer eros dui, tempor ac ipsum a, consectetur facilisis sem. Proin placerat porttitor velit sed mattis. Suspendisse ut erat orci. In hac habitasse platea dictumst. Aenean cursus maximus odio, vel pharetra leo condimentum vel. In nec molestie massa. Suspendisse a tellus ultrices massa laoreet facilisis ac ultricies neque. Curabitur fringilla nunc sed interdum fermentum. Etiam egestas maximus arcu nec dictum. Integer ornare ligula ipsum, sit amet consequat justo euismod porta. Suspendisse a quam lorem. Donec ac ornare tortor. Suspendisse leo tortor, fringilla ut imperdiet ac, pulvinar nec eros. Etiam dignissim mauris sapien, vitae pulvinar nibh placerat dignissim. Pellentesque vitae vulputate nisl. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam elementum tempor lorem, blandit aliquam ipsum commodo id. Nam dictum iaculis neque, quis tempus tortor luctus sit amet. Suspendisse porta enim purus, sit amet accumsan ligula molestie quis. Cras rhoncus ultricies odio. Aliquam imperdiet dapibus aliquet. Curabitur et ullamcorper eros. Fusce ut massa sit amet dolor suscipit tincidunt. Phasellus at tincidunt massa. Praesent ac imperdiet est, ac laoreet libero. Ut in turpis velit. Morbi non diam dui.</p>', NULL, '2020-12-09 22:21:05', '2020-12-16 05:44:00'),
(2, 'Métodos de pago', 'Quisque semper turpis in erat cursus, id auctor nisi sollicitudin. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer at blandit lectus. Pellentesque aliquet velit sem, vitae mollis eros tempor vel. Duis id orci in nulla viverra dignissim at a sem. Mauris iaculis nisl nec enim rhoncus iaculis. Curabitur dapibus fringilla quam, sed blandit ipsum accumsan nec. Donec ac elit lobortis purus sagittis convallis quis et est. Praesent vitae sagittis felis, ac sagittis tortor. Cras tortor lectus, molestie consequat ipsum id, efficitur ullamcorper felis. Sed sapien ipsum, rutrum a odio id, gravida ultrices neque. Nullam finibus mi vel ante dignissim auctor.\r\n\r\nIn nec diam in ipsum dictum auctor quis sit amet sapien. Mauris augue enim, volutpat a malesuada id, hendrerit vitae neque. Aliquam erat volutpat. Etiam ut finibus neque. Nulla et finibus felis. Etiam vestibulum orci id nisl iaculis sodales. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce fermentum rutrum mi non faucibus. Nulla molestie urna eu orci malesuada dictum. Integer eros dui, tempor ac ipsum a, consectetur facilisis sem. Proin placerat porttitor velit sed mattis. Suspendisse ut erat orci. In hac habitasse platea dictumst.\r\n\r\nAenean cursus maximus odio, vel pharetra leo condimentum vel. In nec molestie massa. Suspendisse a tellus ultrices massa laoreet facilisis ac ultricies neque. Curabitur fringilla nunc sed interdum fermentum. Etiam egestas maximus arcu nec dictum. Integer ornare ligula ipsum, sit amet consequat justo euismod porta. Suspendisse a quam lorem. Donec ac ornare tortor. Suspendisse leo tortor, fringilla ut imperdiet ac, pulvinar nec eros. Etiam dignissim mauris sapien, vitae pulvinar nibh placerat dignissim. Pellentesque vitae vulputate nisl. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam elementum tempor lorem, blandit aliquam ipsum commodo id. Nam dictum iaculis neque, quis tempus tortor luctus sit amet.', NULL, '2020-12-09 22:21:41', '2020-12-09 22:21:41'),
(3, 'Devoluciones y envío', 'Quisque semper turpis in erat cursus, id auctor nisi sollicitudin. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer at blandit lectus. Pellentesque aliquet velit sem, vitae mollis eros tempor vel. Duis id orci in nulla viverra dignissim at a sem. Mauris iaculis nisl nec enim rhoncus iaculis. Curabitur dapibus fringilla quam, sed blandit ipsum accumsan nec. Donec ac elit lobortis purus sagittis convallis quis et est. Praesent vitae sagittis felis, ac sagittis tortor. Cras tortor lectus, molestie consequat ipsum id, efficitur ullamcorper felis. Sed sapien ipsum, rutrum a odio id, gravida ultrices neque. Nullam finibus mi vel ante dignissim auctor.\r\n\r\nIn nec diam in ipsum dictum auctor quis sit amet sapien. Mauris augue enim, volutpat a malesuada id, hendrerit vitae neque. Aliquam erat volutpat. Etiam ut finibus neque. Nulla et finibus felis. Etiam vestibulum orci id nisl iaculis sodales. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce fermentum rutrum mi non faucibus. Nulla molestie urna eu orci malesuada dictum. Integer eros dui, tempor ac ipsum a, consectetur facilisis sem. Proin placerat porttitor velit sed mattis. Suspendisse ut erat orci. In hac habitasse platea dictumst.\r\n\r\nAenean cursus maximus odio, vel pharetra leo condimentum vel. In nec molestie massa. Suspendisse a tellus ultrices massa laoreet facilisis ac ultricies neque. Curabitur fringilla nunc sed interdum fermentum. Etiam egestas maximus arcu nec dictum. Integer ornare ligula ipsum, sit amet consequat justo euismod porta. Suspendisse a quam lorem. Donec ac ornare tortor. Suspendisse leo tortor, fringilla ut imperdiet ac, pulvinar nec eros. Etiam dignissim mauris sapien, vitae pulvinar nibh placerat dignissim. Pellentesque vitae vulputate nisl. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam elementum tempor lorem, blandit aliquam ipsum commodo id. Nam dictum iaculis neque, quis tempus tortor luctus sit amet.\r\n\r\nSuspendisse porta enim purus, sit amet accumsan ligula molestie quis. Cras rhoncus ultricies odio. Aliquam imperdiet dapibus aliquet. Curabitur et ullamcorper eros. Fusce ut massa sit amet dolor suscipit tincidunt. Phasellus at tincidunt massa. Praesent ac imperdiet est, ac laoreet libero. Ut in turpis velit. Morbi non diam dui.', NULL, '2020-12-09 22:21:41', '2020-12-09 22:21:41'),
(4, 'Términos y condiciones', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin id nulla ac libero viverra laoreet. Duis varius scelerisque nunc at feugiat. Sed viverra est non fringilla pellentesque. Sed dictum suscipit tristique. In ultricies neque vel aliquam pharetra. Aliquam magna dolor, accumsan a mi id, commodo consequat purus. Nullam lobortis erat a tempor blandit.\r\n\r\nIn nec diam in ipsum dictum auctor quis sit amet sapien. Mauris augue enim, volutpat a malesuada id, hendrerit vitae neque. Aliquam erat volutpat. Etiam ut finibus neque. Nulla et finibus felis. Etiam vestibulum orci id nisl iaculis sodales. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce fermentum rutrum mi non faucibus. Nulla molestie urna eu orci malesuada dictum. Integer eros dui, tempor ac ipsum a, consectetur facilisis sem. Proin placerat porttitor velit sed mattis. Suspendisse ut erat orci. In hac habitasse platea dictumst.\r\n\r\nAenean cursus maximus odio, vel pharetra leo condimentum vel. In nec molestie massa. Suspendisse a tellus ultrices massa laoreet facilisis ac ultricies neque. Curabitur fringilla nunc sed interdum fermentum. Etiam egestas maximus arcu nec dictum. Integer ornare ligula ipsum, sit amet consequat justo euismod porta. Suspendisse a quam lorem. Donec ac ornare tortor. Suspendisse leo tortor, fringilla ut imperdiet ac, pulvinar nec eros. Etiam dignissim mauris sapien, vitae pulvinar nibh placerat dignissim. Pellentesque vitae vulputate nisl. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam elementum tempor lorem, blandit aliquam ipsum commodo id. Nam dictum iaculis neque, quis tempus tortor luctus sit amet.', NULL, '2020-12-09 22:22:06', '2020-12-09 22:22:06'),
(5, 'Garantía', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin id nulla ac libero viverra laoreet. Duis varius scelerisque nunc at feugiat. Sed viverra est non fringilla pellentesque. Sed dictum suscipit tristique. In ultricies neque vel aliquam pharetra. Aliquam magna dolor, accumsan a mi id, commodo consequat purus. Nullam lobortis erat a tempor blandit.</p>\r\n<p>In nec diam in ipsum dictum auctor quis sit amet sapien. Mauris augue enim, volutpat a malesuada id, hendrerit vitae neque. Aliquam erat volutpat. Etiam ut finibus neque. Nulla et finibus felis. Etiam vestibulum orci id nisl iaculis sodales. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce fermentum rutrum mi non faucibus. Nulla molestie urna eu orci malesuada dictum. Integer eros dui, tempor ac ipsum a, consectetur facilisis sem. Proin placerat porttitor velit sed mattis. Suspendisse ut erat orci. In hac habitasse platea dictumst.</p>\r\n<p>Aenean cursus maximus odio, vel pharetra leo condimentum vel. In nec molestie massa. Suspendisse a tellus ultrices massa laoreet facilisis ac ultricies neque. Curabitur fringilla nunc sed interdum fermentum. Etiam egestas maximus arcu nec dictum. Integer ornare ligula ipsum, sit amet consequat justo euismod porta. Suspendisse a quam lorem. Donec ac ornare tortor. Suspendisse leo tortor, fringilla ut imperdiet ac, pulvinar nec eros. Etiam dignissim mauris sapien, vitae pulvinar nibh placerat dignissim. Pellentesque vitae vulputate nisl. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam elementum tempor lorem, blandit aliquam ipsum commodo id. Nam dictum iaculis neque, quis tempus tortor luctus sit amet.</p>', NULL, NULL, '2020-12-19 01:05:09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sku` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titulo_es` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion_es` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `min_descripcion_es` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titulo_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `descripcion_en` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `min_descripcion_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coti` tinyint(1) NOT NULL DEFAULT 0,
  `textura` tinyint(1) DEFAULT 0,
  `precio` double(9,2) DEFAULT 0.00,
  `descuento` double(9,2) DEFAULT 0.00,
  `med_alt` double(6,2) DEFAULT NULL,
  `med_anc` double(6,2) DEFAULT NULL,
  `med_lar` double(6,2) DEFAULT NULL,
  `categoria` int(11) DEFAULT NULL,
  `metaDescripcion` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `llave` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `inicio` tinyint(1) NOT NULL DEFAULT 0,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  `orden` int(11) NOT NULL DEFAULT 99,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `sku`, `titulo_es`, `descripcion_es`, `min_descripcion_es`, `titulo_en`, `descripcion_en`, `min_descripcion_en`, `coti`, `textura`, `precio`, `descuento`, `med_alt`, `med_anc`, `med_lar`, `categoria`, `metaDescripcion`, `llave`, `inicio`, `activo`, `orden`, `created_at`, `updated_at`) VALUES
(1, 'test', 'test', '<p>test</p>', 'test', NULL, NULL, NULL, 0, 0, 1000.00, 10.00, 60.00, 60.00, 123.00, 3, NULL, '475798105', 1, 1, 99, '2021-01-20 22:07:35', '2021-01-28 04:50:24'),
(2, 'test 2', 'test 2', '<p>test 2</p>', 'test 2', NULL, NULL, NULL, 1, 0, 0.00, 0.00, 60.00, 20.00, 70.00, 3, NULL, '4626898407', 1, 1, 99, '2021-01-20 22:13:31', '2021-01-30 11:26:00'),
(3, 'test 3', 'test 3', '<p>test 3</p>', 'test 3', NULL, NULL, NULL, 0, 1, 1500.00, 0.00, 60.00, 20.00, 70.00, 3, NULL, '87708339', 0, 1, 99, '2021-01-20 22:54:01', '2021-01-28 07:58:30');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos_photos`
--

CREATE TABLE `productos_photos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `producto` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orden` int(11) NOT NULL DEFAULT 99
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `productos_photos`
--

INSERT INTO `productos_photos` (`id`, `producto`, `image`, `orden`) VALUES
(1, 1, 'HW3DfieIPNt1uS3t5YnflbRD4BrI8f.jpg', 99),
(2, 1, 'zqGr4W5JEBxmPAM4fdowawEQejXLDB.jpg', 99),
(3, 1, 'MTnBp9Aw58xHWD4WzThYUrnaFx3kXV.jpg', 99);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto_relacions`
--

CREATE TABLE `producto_relacions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `producto` bigint(20) UNSIGNED NOT NULL,
  `otroProducto` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `producto_relacions`
--

INSERT INTO `producto_relacions` (`id`, `producto`, `otroProducto`) VALUES
(18, 2, 1),
(19, 2, 3),
(20, 1, 2),
(21, 3, 3),
(22, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto_versions`
--

CREATE TABLE `producto_versions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `coltex` bigint(20) UNSIGNED NOT NULL,
  `producto` bigint(20) UNSIGNED NOT NULL,
  `precio` double(9,2) DEFAULT NULL,
  `existencia` int(11) DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  `orden` int(11) NOT NULL DEFAULT 99,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `producto_versions`
--

INSERT INTO `producto_versions` (`id`, `coltex`, `producto`, `precio`, `existencia`, `activo`, `orden`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, 2, 1, 99, '2021-01-20 22:07:35', '2021-01-28 05:29:19'),
(2, 6, 1, NULL, 6, 1, 99, '2021-01-20 22:07:35', '2021-01-26 23:18:34'),
(3, 1, 2, NULL, NULL, 1, 99, '2021-01-20 22:13:31', '2021-01-20 22:13:31'),
(4, 6, 2, NULL, NULL, 1, 99, '2021-01-20 22:13:31', '2021-01-20 22:13:31'),
(5, 2, 3, NULL, NULL, 1, 99, '2021-01-20 22:54:01', '2021-01-20 22:54:01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto_version_photos`
--

CREATE TABLE `producto_version_photos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `version` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orden` int(11) NOT NULL DEFAULT 99
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `producto_version_photos`
--

INSERT INTO `producto_version_photos` (`id`, `version`, `image`, `orden`) VALUES
(1, 1, '0bjWwgP7ES6ET72wXJGTgA8I0NR0PN.jpg', 99),
(2, 2, 'atfAslEITkWfZGymz0e50adus7AtCw.jpg', 99),
(3, 1, 'VTaqv7zrScXwR5c7jGb6NYTvhNj2W3.jpg', 99),
(4, 2, 's1PlrSm52OS0qoyqF9NTtmTEtJ5LSb.jpg', 99);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pujas`
--

CREATE TABLE `pujas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `subasta` bigint(20) UNSIGNED NOT NULL,
  `user` bigint(20) UNSIGNED DEFAULT NULL,
  `puja` double(9,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `pujas`
--

INSERT INTO `pujas` (`id`, `subasta`, `user`, `puja`, `created_at`, `updated_at`) VALUES
(1, 3, NULL, 1600.00, '2021-01-15 01:57:06', '2021-01-15 01:57:06'),
(2, 3, 2, 1700.00, '2021-01-15 02:00:26', '2021-01-15 02:00:26'),
(3, 1, NULL, 55.00, '2021-01-15 02:00:47', '2021-01-15 02:00:47'),
(4, 1, NULL, 60.00, '2021-01-15 02:00:56', '2021-01-15 02:00:56'),
(5, 3, NULL, 1800.00, '2021-01-15 02:18:13', '2021-01-15 02:18:13'),
(6, 3, 2, 1900.00, '2021-01-15 03:38:39', '2021-01-15 03:38:39'),
(7, 1, NULL, 65.00, '2021-01-15 03:40:02', '2021-01-15 03:40:02'),
(8, 1, 2, 80.00, '2021-01-15 03:40:15', '2021-01-15 03:40:15'),
(9, 3, NULL, 2000.00, '2021-01-15 09:32:19', '2021-01-15 09:32:19'),
(10, 4, NULL, 3600.00, '2021-01-15 09:33:20', '2021-01-15 09:33:20'),
(11, 2, 2, 1600.00, '2021-01-17 05:47:08', '2021-01-17 05:47:08'),
(12, 2, NULL, 1700.00, '2021-01-17 05:47:16', '2021-01-17 05:47:16'),
(13, 2, NULL, 2000.00, '2021-01-19 03:39:27', '2021-01-19 03:39:27'),
(14, 2, 2, 2100.00, '2021-01-19 12:06:50', '2021-01-19 12:06:50'),
(15, 2, NULL, 2200.00, '2021-01-20 03:33:55', '2021-01-20 03:33:55'),
(16, 3, NULL, 2100.00, '2021-01-20 01:42:05', '2021-01-20 01:42:05'),
(17, 3, 2, 2200.00, '2021-01-22 02:36:13', '2021-01-22 02:36:13'),
(18, 3, 1, 2300.00, '2021-01-22 02:37:54', '2021-01-22 02:37:54'),
(19, 2, 5, 2300.00, '2021-01-22 13:51:00', '2021-01-22 13:51:00'),
(20, 2, 2, 2400.00, '2021-01-22 13:51:44', '2021-01-22 13:51:44'),
(21, 9, 1, 7.00, '2021-01-30 05:53:38', '2021-01-30 05:53:38'),
(22, 9, 1, 9.00, '2021-01-30 05:53:48', '2021-01-30 05:53:48'),
(23, 9, 1, 11.00, '2021-01-30 05:53:50', '2021-01-30 05:53:50'),
(24, 9, 1, 13.00, '2021-01-30 06:22:56', '2021-01-30 06:22:56'),
(25, 9, 1, 15.00, '2021-01-30 07:43:00', '2021-01-30 07:43:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seccions`
--

CREATE TABLE `seccions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `seccion` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `portada` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `seccions`
--

INSERT INTO `seccions` (`id`, `seccion`, `portada`) VALUES
(1, 'index', NULL),
(2, 'about', NULL),
(3, 'productos', NULL),
(4, 'subastas', NULL),
(5, 'contacto', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sizes`
--

CREATE TABLE `sizes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `size` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `catSize` bigint(20) UNSIGNED NOT NULL,
  `orden` int(11) NOT NULL DEFAULT 99,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `sizes`
--

INSERT INTO `sizes` (`id`, `size`, `catSize`, `orden`, `created_at`, `updated_at`) VALUES
(1, '1', 1, 3, '2020-12-24 23:51:58', '2020-12-25 00:08:22'),
(2, '1 1/2', 1, 4, '2020-12-24 23:56:24', '2020-12-25 00:08:22'),
(3, '3/4', 1, 2, '2020-12-24 23:56:35', '2020-12-25 00:08:22'),
(4, '1/2', 1, 1, '2020-12-24 23:56:42', '2020-12-25 00:08:19'),
(5, '1/4', 1, 0, '2020-12-24 23:57:06', '2020-12-25 00:08:08'),
(6, '2', 1, 5, '2020-12-24 23:57:17', '2020-12-25 00:08:09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subastas`
--

CREATE TABLE `subastas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `titulo_es` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion_es` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `min_descripcion_es` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titulo_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `descripcion_en` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `min_descripcion_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `precio_inicial` double(9,2) NOT NULL,
  `precio_actual` double(9,2) DEFAULT NULL,
  `puja_min` double(9,2) NOT NULL,
  `deadline` datetime NOT NULL,
  `lastUserId` bigint(20) UNSIGNED NOT NULL,
  `inicio` tinyint(1) NOT NULL DEFAULT 0,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  `orden` int(11) NOT NULL DEFAULT 99,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `subastas`
--

INSERT INTO `subastas` (`id`, `titulo_es`, `descripcion_es`, `min_descripcion_es`, `titulo_en`, `descripcion_en`, `min_descripcion_en`, `precio_inicial`, `precio_actual`, `puja_min`, `deadline`, `lastUserId`, `inicio`, `activo`, `orden`, `created_at`, `updated_at`) VALUES
(1, 'Oximetro Pulso Dedo Hospitalario Hogar Deportivo Oled', '<p>ANTES DE REALIZAR UNA DEVOLUCI&Oacute;N FAVOR DE REVISAR QUE LA POSICI&Oacute;N DE LAS PILAS SEA CORRECTA, DE ACUERDO AL INDICATIVO QUE SE MUESTRA DENTRO DEL OX&Iacute;METRO.<br /><br /><br />* Ox&iacute;metro de pulso uso adulto para dedo, para bater&iacute;as AAA, monitor de frecuencia card&iacute;aca de ox&iacute;geno en sangre (Spo2), pulsioximetro juvenil, compacto y ligero con un est&iacute;lo din&aacute;mico,<br /><br />* Ox&iacute;metro de pulso adecuado para hogares, hospitales, escuelas, centros de examen f&iacute;sico, y uso dom&eacute;stico.<br /><br />* Monitores ligeros que se conectan a la punta de un dedo para controlar la cantidad de ox&iacute;geno que se lleva en el cuerpo.<br /><br />Caracter&iacute;sticas:<br /><br />1. El producto adopta una pantalla<br />con 2 posiciones<br />2. Bajo consumo de energ&iacute;a, funciona continuamente durante m&aacute;s de seis horas<br />3. Indicador de la bater&iacute;a.<br />4. El dispositivo se apagar&aacute; autom&aacute;ticamente en 8 segundos<br />si no hay se&ntilde;al de dedo.<br />5. Moderno ,compacto, ligero en peso y c&oacute;modo de llevar.<br />6. Ajuste ergon&oacute;mico censor al dedo.<br />7. Muestra saturaci&oacute;n de ox&iacute;geno en sangre (SpO2) no invasiva.<br /><br />Tipo de pantalla:pantalla SpO2.<br /><br />Rango de medida:70%~99%<br /><br />Precisi&oacute;n:&plusmn;2% entre 80%~99%;<br />&plusmn;3% (cuando el valor de SpO2<br />est&aacute; entre 70%~79%)<br />Por debajo de 70% sin requisitos.<br /><br />Resoluci&oacute;n:&plusmn;1%<br />FP:Rango de medida:30BPM~240BPM<br /><br />Precisi&oacute;n:&plusmn;1BPM o &plusmn;1%(el m&aacute;s grande)<br /><br />Poder: dos bater&iacute;as alcalinas AAA 1.5V<br /><br />Consumo de poder:por debajo de 30mA<br /><br />Apagado autom&aacute;tico: el dispositivo se apaga por si<br />mismo cuando no se coloca ning&uacute;n dedo por m&aacute;s<br />de 8 segundos.<br /><br />Dimensiones:aprox. 58mm&times;36mm&times;33mm<br /><br />Ambiente de Operaci&oacute;n: Temperatura de<br />operaci&oacute;n:5&iquest;~40&iquest;<br /><br />Temperatura de almacenamiento:-10&iquest;~40&iquest;<br /><br />Humedad Ambiental:15%~80% en operaci&oacute;n<br />10%~80% en almacenamiento<br /><br />Presi&oacute;n del Aire: 86kPa~106kPa<br /><br />Declaraci&oacute;n:El EMC de este producto est&aacute;<br />en concordancia con el est&aacute;ndar IEC60601-1-2.</p>', 'Oxímetro de pulso uso adulto para dedo', NULL, NULL, NULL, 50.00, 80.00, 5.00, '2021-01-13 12:35:00', 0, 0, 1, 99, '2021-01-11 17:33:36', '2021-01-15 03:40:15'),
(2, 'Disco Duro 3TB WD Purple/ Sata 3 / Sistemas Circuito Cerrado / WD30PURX', '<p>Disco Duro 3TB WD Purple/ Sata 3 / Sistemas Circuito Cerrado / WD30PURX</p>', 'Disco Duro 3TB WD Purple/ Sata 3 / Sistemas Circuito Cerrado / WD30PURX', NULL, NULL, NULL, 1500.00, 2400.00, 100.00, '2021-01-24 12:35:00', 0, 0, 1, 99, '2021-01-15 01:05:22', '2021-01-22 13:51:44'),
(3, 'Disco Duro 3TB WD Purple/ Sata 3 / Sistemas Circuito Cerrado / WD30PURX', '<p>Disco Duro 3TB WD Purple/ Sata 3 / Sistemas Circuito Cerrado / WD30PURX</p>', 'Disco Duro 3TB WD Purple/ Sata 3 / Sistemas Circuito Cerrado / WD30PURX', NULL, NULL, NULL, 1500.00, 2300.00, 100.00, '2021-01-24 12:35:00', 0, 0, 1, 99, '2021-01-15 01:06:39', '2021-01-22 02:37:54'),
(4, 'Guitar Hero Live Para Play Station 4 Ps4 Nuevo Sellado', '<p>.:: Guitar Hero Live Para Play Station 4 Ps4 Nuevo Sellado ::..<br /><br />Tu compra incluye:<br />Juego Guitar Hero live Para PS4<br />1 Guitarra Guitar Hero<br />1 Receptores<br />1 Thali<br />Instructivo<br />Garantia<br /><br />***NOTA: LAS CAJAS EST&Aacute;N ALGO DA&Ntilde;ADAS POR EL TIEMPO QUE LLEVAN ESTIBADAS Y EN ALGUNOS CASOS LAS PERLAS DE HUMEDAD EST&Aacute;N SUELTAS DENTRO DE LA CAJA , LO QUE PUEDE PROVOCAR RAYONES EN EL EQUIPO, NO AFECTA SU FUNCIONAMIENTO***<br /><br />\"ULTIMAS PIEZAS\"<br /><br />ENTREGAS PERSONALES :<br />Mercado Libre quito la opcion de entregas personales.<br /><br />HORARIO:<br />Lunes a Viernes 11 a 7<br />Sabado 11 a 3<br /><br />ENV&Iacute;OS:<br />De Lunes a Viernes por la mensajer&iacute;a designada por Mercado Env&iacute;os.<br />Tiempo aprox de entrega de 1 a 3 d&iacute;as dependiendo la ciudad.<br /><br />Despachamos su paquete el mismo d&iacute;a si su pago es acreditado antes de las 14:00 hrs<br /><br />&bull; Pagos acreditados despu&eacute;s de la hora se&ntilde;alada se enviaran al siguiente d&iacute;a h&aacute;bil<br /><br />&bull; Pedidos en fin de semana se env&iacute;an al siguiente d&iacute;a h&aacute;bil.<br />Nota: Como vendedor no puedo elegir la paqueter&iacute;a que se usar&aacute; en los env&iacute;os. Una vez entregado el paquete a la mensajer&iacute;a es competencia de la misma el tiempo de entrega.<br /><br />Estamos a Sus Ordenes!!!<br /><br />Bleshop Games</p>', 'Guitar Hero Live Para Play Station 4 Ps4 Nuevo Sellado rapida', NULL, NULL, NULL, 3400.00, 3600.00, 200.00, '2021-01-17 12:35:00', 0, 0, 1, 99, '2021-01-15 03:43:46', '2021-01-15 09:33:20'),
(5, 'pau', '<p>dos cosas</p>', 'pau pau', NULL, NULL, NULL, 3000.00, NULL, 200.00, '2021-01-17 12:35:00', 0, 0, 1, 99, '2021-01-17 03:22:22', '2021-01-20 21:02:40'),
(6, 'yahir', '<p>ddddd</p>', 'ikkkj', NULL, NULL, NULL, 5000.00, NULL, 400.00, '2021-01-17 12:35:00', 0, 0, 1, 99, '2021-01-17 05:49:15', '2021-01-17 05:49:15'),
(7, 'test', '<p>test</p>', 'test', NULL, NULL, NULL, 1500.00, NULL, 20.00, '2021-01-22 12:35:00', 0, 0, 1, 99, '2021-01-22 14:02:43', '2021-01-22 14:02:43'),
(8, 'test 2', '<p>test 2</p>', 'test 2', NULL, NULL, NULL, 2000.00, NULL, 100.00, '2021-01-26 12:35:00', 0, 0, 1, 99, '2021-01-22 14:03:49', '2021-01-22 14:03:49'),
(9, 'chetos', '<p>chetos</p>', 'chetos', NULL, NULL, NULL, 5.00, 15.00, 2.00, '2021-01-29 21:35:00', 0, 0, 1, 99, '2021-01-30 05:52:41', '2021-01-30 07:43:00'),
(10, 'pizza', '<p>pizza</p>', 'pizza', NULL, NULL, NULL, 100.00, NULL, 20.00, '2021-01-29 22:35:00', 0, 0, 1, 99, '2021-01-30 07:11:00', '2021-01-30 07:11:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subastas_photos`
--

CREATE TABLE `subastas_photos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `subasta` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orden` int(11) NOT NULL DEFAULT 99
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `subastas_photos`
--

INSERT INTO `subastas_photos` (`id`, `subasta`, `image`, `orden`) VALUES
(1, 1, 'EJK2aApu6hr6HTIq8psfYX8PrHeNys.jpg', 1),
(2, 1, 'RZszQ9lAqylLN8Lyl8R7NEQoSowUZY.jpg', 0),
(3, 3, 'aKB2RTtSZVwdD4yFbSLDhecXzyWaRs.jpg', 99),
(4, 3, '3hhibGRqmIDGFnQRLtBgvjxxdUqGo3.jpg', 99),
(5, 4, 'XrPpzYU1pBl3RIFO7JqZHZM5ywWl4c.png', 99),
(6, 4, 'wlHTG8uXcexkGDaYqDLdNCeqN2xdx2.png', 99),
(7, 5, 'e24WRVvpmtpt4TrhTa6gGaNZ2p53lU.jpg', 99),
(8, 5, '8avOS0lyQ5YhEehPrASug3yDCRFxPp.png', 99),
(13, 6, 'ZHqlcKb7A4554qLiw6X41dJXsApkPP.jpeg', 99),
(14, 5, 's5b1r9WnbLvsXJ78MxwVX0GfeHPpQj.jpeg', 99),
(15, 8, '2FUggGZUPC39tWGQTlwYfevy1XdigB.jpg', 99),
(16, 9, 'tu0m1EZeNwfhnOtObjV5yLwvsrUXtu.png', 99),
(17, 10, 'OvWMD4ORPVbaW7VYPlp3vEuLNboxRT.jpg', 99);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sucursals`
--

CREATE TABLE `sucursals` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nombre` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tel` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `direccion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `cp` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ubicacion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `maps` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lat` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lon` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `sucursals`
--

INSERT INTO `sucursals` (`id`, `nombre`, `tel`, `mail`, `direccion`, `cp`, `ubicacion`, `maps`, `lat`, `lon`) VALUES
(2, 'Nombre de la Sucursal 2', '2345678901', 'contacto@sucursal2.com', '<p>AVENIDA LAPIZL&Aacute;ZULI<br />2074 INT. 3<br />RESIDENCIAL VICTORIA</p>', '23451', 'Oaxaca Mx', NULL, NULL, NULL),
(3, 'Nombre de la Sucursal 3', '3456789012', 'contacto3@sucursal3.com', '<p>AVENIDA NOMBRE DE LA CALLE 3<br />3333 INT. 3<br />COLONIA3</p>', '34512', 'Nuevo Leon Mx', NULL, NULL, NULL),
(5, 'Nombre de la Sucursal 1', '1234567890', 'contacto@sucursal1.com', '<p>Lapizl&aacute;zuli 2074,&nbsp;<br />Interior 3<br />Col. Bosques de la Victoria</p>', '12345', 'Guadalajara, Jal.', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `telefono` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `empresa` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rfc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` int(11) DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `lastname`, `username`, `email`, `email_verified_at`, `telefono`, `facebook`, `empresa`, `rfc`, `role`, `password`, `activo`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'yahir', 'Lopez', '', 'yahir@wozial.com', NULL, '3333333333', NULL, NULL, NULL, NULL, '$2y$10$5rYotMH7gq5ZZWNakKw95..SRr5Q1ly9qtUZxStZZVNyQam4LDGse', 1, NULL, '2021-01-16 14:24:58', '2021-01-29 06:23:08'),
(2, 'yahir', NULL, NULL, 'yahir2@wozial.com', NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$pl01s2tvzbaBL7NBeMTjLuxZ3Ay/hgULJpefvyOVV0eXfNYXb.sc2', 1, NULL, '2021-01-22 01:07:43', '2021-01-22 01:07:43'),
(5, 'yahir', NULL, NULL, 'yahir3@wozial.com', NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$qbFazt6h8smbH/CNXKHIl.pFAyik2y6.yKHq7uBYaolBEoy2cZOva', 1, NULL, '2021-01-22 13:15:01', '2021-01-22 13:15:01'),
(6, 'Adrian', NULL, NULL, 'adrian@wozial.com', NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$neLQY08cIlR6hd/duFZ4FOjTv0j3NaGgyCslCmI1fjznMccd4SHyW', 1, NULL, '2021-01-29 02:18:16', '2021-01-29 02:18:16');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_user_unique` (`user`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indices de la tabla `carrusels`
--
ALTER TABLE `carrusels`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categ_tamanos`
--
ALTER TABLE `categ_tamanos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `colors`
--
ALTER TABLE `colors`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `configuracions`
--
ALTER TABLE `configuracions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cupons`
--
ALTER TABLE `cupons`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cupons_codigo_unique` (`codigo`);

--
-- Indices de la tabla `domicilios`
--
ALTER TABLE `domicilios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `domicilios_user_foreign` (`user`);

--
-- Indices de la tabla `elementos`
--
ALTER TABLE `elementos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `elementos_seccion_foreign` (`seccion`);

--
-- Indices de la tabla `espacioproductos`
--
ALTER TABLE `espacioproductos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `espacioproductos_producto_foreign` (`producto`),
  ADD KEY `espacioproductos_espacio_foreign` (`espacio`);

--
-- Indices de la tabla `espacios`
--
ALTER TABLE `espacios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `facturas`
--
ALTER TABLE `facturas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `facturas_user_foreign` (`user`);

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `galeria_subastas`
--
ALTER TABLE `galeria_subastas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `newslatters`
--
ALTER TABLE `newslatters`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `nosotrosgalerias`
--
ALTER TABLE `nosotrosgalerias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pedidos_usuario_foreign` (`usuario`);

--
-- Indices de la tabla `pedido_detalles`
--
ALTER TABLE `pedido_detalles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pedido_detalles_pedido_foreign` (`pedido`),
  ADD KEY `pedido_detalles_producto_foreign` (`producto`);

--
-- Indices de la tabla `politicas`
--
ALTER TABLE `politicas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `productos_sku_unique` (`sku`),
  ADD UNIQUE KEY `llave` (`llave`);

--
-- Indices de la tabla `productos_photos`
--
ALTER TABLE `productos_photos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `productos_photos_producto_foreign` (`producto`);

--
-- Indices de la tabla `producto_relacions`
--
ALTER TABLE `producto_relacions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `producto_relacions_producto_foreign` (`producto`),
  ADD KEY `producto_relacions_otroproducto_foreign` (`otroProducto`);

--
-- Indices de la tabla `producto_versions`
--
ALTER TABLE `producto_versions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `producto_version_photos`
--
ALTER TABLE `producto_version_photos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `producto_version_photos_version_foreign` (`version`);

--
-- Indices de la tabla `pujas`
--
ALTER TABLE `pujas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pujas_subasta_foreign` (`subasta`),
  ADD KEY `pujas_user_foreign` (`user`);

--
-- Indices de la tabla `seccions`
--
ALTER TABLE `seccions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sizes`
--
ALTER TABLE `sizes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sizes_catsize_foreign` (`catSize`);

--
-- Indices de la tabla `subastas`
--
ALTER TABLE `subastas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `subastas_photos`
--
ALTER TABLE `subastas_photos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `subastas_photos_subasta_foreign` (`subasta`);

--
-- Indices de la tabla `sucursals`
--
ALTER TABLE `sucursals`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_username_unique` (`username`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `carrusels`
--
ALTER TABLE `carrusels`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `categ_tamanos`
--
ALTER TABLE `categ_tamanos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `colors`
--
ALTER TABLE `colors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `configuracions`
--
ALTER TABLE `configuracions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `cupons`
--
ALTER TABLE `cupons`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `domicilios`
--
ALTER TABLE `domicilios`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `elementos`
--
ALTER TABLE `elementos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `espacioproductos`
--
ALTER TABLE `espacioproductos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `espacios`
--
ALTER TABLE `espacios`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `facturas`
--
ALTER TABLE `facturas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `galeria_subastas`
--
ALTER TABLE `galeria_subastas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT de la tabla `newslatters`
--
ALTER TABLE `newslatters`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `nosotrosgalerias`
--
ALTER TABLE `nosotrosgalerias`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pedido_detalles`
--
ALTER TABLE `pedido_detalles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `politicas`
--
ALTER TABLE `politicas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `productos_photos`
--
ALTER TABLE `productos_photos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `producto_relacions`
--
ALTER TABLE `producto_relacions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `producto_versions`
--
ALTER TABLE `producto_versions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `producto_version_photos`
--
ALTER TABLE `producto_version_photos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `pujas`
--
ALTER TABLE `pujas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `seccions`
--
ALTER TABLE `seccions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `sizes`
--
ALTER TABLE `sizes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `subastas`
--
ALTER TABLE `subastas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `subastas_photos`
--
ALTER TABLE `subastas_photos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `sucursals`
--
ALTER TABLE `sucursals`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `domicilios`
--
ALTER TABLE `domicilios`
  ADD CONSTRAINT `domicilios_user_foreign` FOREIGN KEY (`user`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `elementos`
--
ALTER TABLE `elementos`
  ADD CONSTRAINT `elementos_seccion_foreign` FOREIGN KEY (`seccion`) REFERENCES `seccions` (`id`);

--
-- Filtros para la tabla `espacioproductos`
--
ALTER TABLE `espacioproductos`
  ADD CONSTRAINT `espacioproductos_espacio_foreign` FOREIGN KEY (`espacio`) REFERENCES `espacios` (`id`),
  ADD CONSTRAINT `espacioproductos_producto_foreign` FOREIGN KEY (`producto`) REFERENCES `productos` (`id`);

--
-- Filtros para la tabla `facturas`
--
ALTER TABLE `facturas`
  ADD CONSTRAINT `facturas_user_foreign` FOREIGN KEY (`user`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD CONSTRAINT `pedidos_usuario_foreign` FOREIGN KEY (`usuario`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `pedido_detalles`
--
ALTER TABLE `pedido_detalles`
  ADD CONSTRAINT `pedido_detalles_pedido_foreign` FOREIGN KEY (`pedido`) REFERENCES `pedidos` (`id`),
  ADD CONSTRAINT `pedido_detalles_producto_foreign` FOREIGN KEY (`producto`) REFERENCES `productos` (`id`);

--
-- Filtros para la tabla `productos_photos`
--
ALTER TABLE `productos_photos`
  ADD CONSTRAINT `productos_photos_producto_foreign` FOREIGN KEY (`producto`) REFERENCES `productos` (`id`);

--
-- Filtros para la tabla `producto_relacions`
--
ALTER TABLE `producto_relacions`
  ADD CONSTRAINT `producto_relacions_otroproducto_foreign` FOREIGN KEY (`otroProducto`) REFERENCES `productos` (`id`),
  ADD CONSTRAINT `producto_relacions_producto_foreign` FOREIGN KEY (`producto`) REFERENCES `productos` (`id`);

--
-- Filtros para la tabla `producto_version_photos`
--
ALTER TABLE `producto_version_photos`
  ADD CONSTRAINT `producto_version_photos_version_foreign` FOREIGN KEY (`version`) REFERENCES `producto_versions` (`id`);

--
-- Filtros para la tabla `pujas`
--
ALTER TABLE `pujas`
  ADD CONSTRAINT `pujas_subasta_foreign` FOREIGN KEY (`subasta`) REFERENCES `subastas` (`id`),
  ADD CONSTRAINT `pujas_user_foreign` FOREIGN KEY (`user`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `sizes`
--
ALTER TABLE `sizes`
  ADD CONSTRAINT `sizes_catsize_foreign` FOREIGN KEY (`catSize`) REFERENCES `categ_tamanos` (`id`);

--
-- Filtros para la tabla `subastas_photos`
--
ALTER TABLE `subastas_photos`
  ADD CONSTRAINT `subastas_photos_subasta_foreign` FOREIGN KEY (`subasta`) REFERENCES `subastas` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
